package helpers {
public class Resource {
    public function Resource() {
    }

    // --- FONTS

    [Embed(source = "../assets/fonts/HNCN.otf", fontFamily = "HNCN", mimeType = "application/x-font-opentype", embedAsCFF = 'false')] public static var FONT_HNCN:Class;
    [Embed(source = "../assets/fonts/HNCB.otf", fontFamily = "HNCB", mimeType = "application/x-font-opentype", embedAsCFF = 'false')] public static var FONT_HNCB:Class;
    // --- IMAGES

    [Embed(source = '../assets/images/start.png')] public static const start:Class;
    [Embed(source = '../assets/images/close.png')] public static const close:Class;
    [Embed(source = '../assets/images/attempts.png')] public static const attempts:Class;

    [Embed(source = '../assets/images/border_top.png')] public static const border_top:Class;
    [Embed(source = '../assets/images/border_right.png')] public static const border_right:Class;
    [Embed(source = '../assets/images/border_bottom.png')] public static const border_bottom:Class;
    [Embed(source = '../assets/images/border_left.png')] public static const border_left:Class;

    [Embed(source = '../assets/images/buttonRed.png')] public static const buttonRed:Class;
    [Embed(source = '../assets/images/buttonBlue.png')] public static const buttonBlue:Class;

    [Embed(source='../assets/images/borderDarkBlue.png')] public static const borderDarkBlue:Class;
    [Embed(source='../assets/images/borderLightBlue.png')] public static const borderLightBlue:Class;
}
}
