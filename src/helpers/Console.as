package helpers {
	import flash.external.ExternalInterface;
	
	public class Console {
		public function Console() {
		}
		
		private static var time:Number = new Date().time;
		
		public static function log(...args):void {
			var date:Date = new Date(new Date().time - time);
			
			var dateString:String = '[' + date.getUTCHours() + ':' + date.getUTCMinutes() + ':' + date.getUTCSeconds() + '.'+ date.getUTCMilliseconds() + '] > ';
			
			args.unshift(dateString);
			
			if(ExternalInterface.available) {
				try {
					ExternalInterface.call('function(args) { if(window && window.console && window.console.log) { window.console.log(args); }  }', args);		
				} 
				catch(error:Error) {
					
				}
			}		
		} 
		
	}
	
}