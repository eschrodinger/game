package helpers {
import flash.filters.ColorMatrixFilter;

public class Constants {
    public function Constants() {
    }

    public static const BLACKOUT_FILTER:ColorMatrixFilter = new ColorMatrixFilter([0.3086, 0.6094, 0.082, 0, 0, 0.3086, 0.6094, 0.082, 0, 0, 0.3086, 0.6094, 0.082, 0, 0, 0, 0, 0, 1, 0]);
    public static const CONTRAST_FILTER:ColorMatrixFilter = new ColorMatrixFilter([1.28, 0, 0, 0, -17.78, 0, 1.28, 0, 0, -17.78, 0, 0, 1.28, 0, -17.78, 0, 0, 0, 1, 0]);

    public static const BG_GAME:uint = 0xFCEAB0;
    public static const BG_DIALOG_OVERLAY:uint = 0xC4CCB7;

    public static const COUNT_ATTEMPTS:int = 5;
    public static const DEFAULT_ATTEMPTS:int = 5;

}
}
