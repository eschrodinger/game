package classes.events {
import classes.base.BaseEvent;

import flash.events.Event;

public class ContainerEvent extends BaseEvent {
		
		public static const CONTENT_WIDTH_CHANGE:String = "ContainerEvent:CONTENT_WIDTH_CHANGE";
		public static const CONTENT_HEIGHT_CHANGE:String = "ContainerEvent:CONTENT_HEIGHT_CHANGE";
		
		/**
		 * Изменение ширины/высоты 
		 */		
		public static const CONTENT_SIZE_CHANGE:String = "ContainerEvent:CONTENT_SIZE_CHANGE";
		
		/**
		 * Изменение ширины/высоты + радиуса
		 */	
		public static const CONTENT_FORM_CHANGE:String = "ContainerEvent:CONTENT_FORM_CHANGE";
		
		/**
		 * Пересчет позиции
		 */	
		public static const CONTENT_UPDATE:String = "ContainerEvent:CONTENT_UPDATE";
		
		public static const CONTENT_CHILD_ADDED:String = "ContainerEvent:CONTENT_CHILD_ADDED";
		public static const CONTENT_CHILD_REMOVED:String = "ContainerEvent:CONTENT_CHILD_REMOVED";
		public static const CONTENT_CHILDS_COUNT_CHANGE:String = "ContainerEvent:CONTENT_CHILDS_COUNT_CHANGE";
		
		public function ContainerEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false) {
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event { 
			var newEvent:ContainerEvent = new  ContainerEvent(type, bubbles, cancelable);
			
			return newEvent;
		} 
	}
}