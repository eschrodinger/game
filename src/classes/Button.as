package classes {
import classes.containers.AutoContainer;
import classes.containers.Container;
import classes.containers.CornerContainer;
import classes.utils.AlignUtil;
import classes.utils.SpecialUtil;

import flash.display.DisplayObject;
import flash.events.MouseEvent;

public class Button extends CornerContainer {

    public static const STATE_UP:String = "STATE_UP";
    public static const STATE_OVER:String = "STATE_OVER";
    public static const STATE_DOWN:String = "STATE_DOWN";
    public static const STATE_DISABLED:String = "STATE_DISABLED";
    public static const STATE_CHECKED:String = "STATE_CHECKED";

    // ---

    private var _buttonAttributes:Boolean = false;

    private var _hasMouseOver:Boolean = false;
    private var _hasMouseDown:Boolean = false;

    private var _stateUp:Container;
    private var _stateOver:Container;
    private var _stateDown:Container;
    private var _stateDisabled:Container;
    private var _stateChecked:Container;

    private var _enabled:Boolean = true;
    private var _checked:Boolean = false;

    private var _state:String;

    // --- Действия для текстовых полей

    private var _text:String;
    private var _icon:Class;

    private var _textAlign:String;
    private var _iconAlign:String;

    private var _color:Object;
    private var _font:String;
    private var _underline:Object;
    private var _size:Object;
    private var _bold:Object;
    private var _wordWrap:Boolean;

    private var _iconOffsetX:int;
    private var _iconOffsetY:int;

    private var _labelOffsetX:int;
    private var _labelOffsetY:int;

    private var _iconUp:AutoContainer;
    private var _iconOver:AutoContainer;
    private var _iconDown:AutoContainer;
    private var _iconDisabled:AutoContainer;
    private var _iconChecked:AutoContainer;

    private var _labelUp:TextLabel;
    private var _labelOver:TextLabel;
    private var _labelDown:TextLabel;
    private var _labelDisabled:TextLabel;
    private var _labelChecked:TextLabel;

    protected var icons:Object;
    protected var labels:Object;

    // --- Хранение данных

    public var data:*;

    public function Button(_width:int, _height:int, _corner:int) {
        super(_width, _height, _corner);

        // ---

        tabEnabled = false;
        tabEnabled = false;

        useHandCursor = true;
        buttonMode = true;

        mouseChildren = true;
        mouseEnabled = true;

        masked = false;

        cacheAsBitmap = true;

        // ---

        addEventListener(MouseEvent.CLICK, click_Handler);

        addEventListener(MouseEvent.MOUSE_DOWN, mouseDown_Handler);
        addEventListener(MouseEvent.MOUSE_UP, mouseUp_Handler);
        addEventListener(MouseEvent.MOUSE_OVER, mouseOver_Handler);
        addEventListener(MouseEvent.MOUSE_OUT, mouseOut_Handler);

        // ---

        _iconUp = addChild(new AutoContainer()) as AutoContainer;
        _iconUp.mouseEnabled = false;
        _iconUp.mouseChildren = false;

        _iconOver = addChild(new AutoContainer()) as AutoContainer;
        _iconOver.mouseEnabled = false;
        _iconOver.mouseChildren = false;

        _iconDown = addChild(new AutoContainer()) as AutoContainer;
        _iconDown.mouseEnabled = false;
        _iconDown.mouseChildren = false;

        _iconChecked = addChild(new AutoContainer()) as AutoContainer;
        _iconChecked.mouseEnabled = false;
        _iconChecked.mouseChildren = false;

        _iconDisabled = addChild(new AutoContainer()) as AutoContainer;
        _iconDisabled.mouseEnabled = false;
        _iconDisabled.mouseChildren = false;

        // ---

        _labelUp = addChild(new TextLabel) as TextLabel;
        _labelUp.mouseEnabled = false;
        _labelUp.mouseChildren = false;

        _labelOver = addChild(new TextLabel) as TextLabel;
        _labelOver.mouseEnabled = false;
        _labelOver.mouseChildren = false;

        _labelDown = addChild(new TextLabel) as TextLabel;
        _labelDown.mouseEnabled = false;
        _labelDown.mouseChildren = false;

        _labelDisabled = addChild(new TextLabel) as TextLabel;
        _labelDisabled.mouseEnabled = false;
        _labelDisabled.mouseChildren = false;

        _labelChecked = addChild(new TextLabel) as TextLabel;
        _labelChecked.mouseEnabled = false;
        _labelChecked.mouseChildren = false;

        // ---

        icons = {};
        icons[STATE_UP] = _iconUp;
        icons[STATE_OVER] = _iconOver;
        icons[STATE_DOWN] = _iconDown;
        icons[STATE_DISABLED] = _iconDisabled;
        icons[STATE_CHECKED] = _iconChecked;

        labels = {};
        labels[STATE_UP] = _labelUp;
        labels[STATE_OVER] = _labelOver;
        labels[STATE_DOWN] = _labelDown;
        labels[STATE_DISABLED] = _labelDisabled;
        labels[STATE_CHECKED] = _labelChecked;

        // ---

        state = STATE_UP;
    }

    // ---

    override public function initialize():void {
        stage.addEventListener(MouseEvent.MOUSE_UP, stage_MouseUp_Handler);
    }

    override public function debug(value:Boolean, debugColor:int=0x000000, debugAlpha:Number=0.5):void {
        super.debug(value, debugColor, debugAlpha);

        SpecialUtil.foreach(labels, function(key:String, label:TextLabel):void {
            label.debug(value, debugColor, debugAlpha);
        });
    }

    // ---

    public function get textAlign():String {
        return _textAlign;
    }

    public function set textAlign(value:String):void {
        _textAlign = value;

        update();
    }

    // ---

    public function get iconAlign():String {
        return _iconAlign;
    }

    public function set iconAlign(value:String):void {
        _iconAlign = value;

        update();
    }

    // ---

    public function get buttonAttributes():Boolean {
        return _buttonAttributes;
    }

    public function set buttonAttributes(value:Boolean):void {
        _buttonAttributes = value;

        if(_enabled) {
            useHandCursor = true;
            buttonMode = true;
        }
    }

    // ---

    public function get labelChecked():TextLabel {
        return _labelChecked;
    }

    public function get labelDisabled():TextLabel {
        return _labelDisabled;
    }

    public function get labelDown():TextLabel {
        return _labelDown;
    }

    public function get labelOver():TextLabel {
        return _labelOver;
    }

    public function get labelUp():TextLabel {
        return _labelUp;
    }

    // ---

    public function get iconChecked():AutoContainer {
        return _iconChecked;
    }

    public function get iconDisabled():AutoContainer {
        return _iconDisabled;
    }

    public function get iconDown():AutoContainer {
        return _iconDown;
    }

    public function get iconOver():AutoContainer {
        return _iconOver;
    }

    public function get iconUp():AutoContainer {
        return _iconUp;
    }

    // ---

    public function get hasMouseDown():Boolean {
        return _hasMouseDown;
    }

    public function get hasMouseOver():Boolean {
        return _hasMouseOver;
    }

    public function get state():String {
        return _state;
    }

    public function set state(value:String):void {
        _state = value;

        update();
    }

    public function get checked():Boolean {
        return _checked;
    }

    public function set checked(value:Boolean):void {
        if(!_enabled) {
            return;
        }

        _checked = value;

        if(value) {
            state = STATE_CHECKED;
        } else {
            state = STATE_UP;
        }
    }

    public function get enabled():Boolean {
        return _enabled;
    }

    public function set enabled(value:Boolean):void {
        _enabled = value;


        if(!_buttonAttributes) {
            useHandCursor = value;
            buttonMode = value;
            mouseEnabled = value;
        }

        if(value) {
            if(hasMouseOver) {
                state = STATE_OVER;
            } else {
                state = STATE_UP;
            }
        } else {
            state = STATE_DISABLED;
        }
    }


    // ---

    public function get stateUp():DisplayObject {
        return _stateUp;
    }

    public function set stateUp(value:DisplayObject):void {
        if(_stateUp && _stateUp.parent == this) {
            removeChild(_stateUp);
        }

        _stateUp = addChild(value) as Container;
        _stateUp.mouseChildren = false;

        // ---

        update();
    }

    // ---

    public function get stateOver():DisplayObject {
        return _stateOver;
    }

    public function set stateOver(value:DisplayObject):void {
        if(_stateOver && _stateOver.parent == this) {
            removeChild(_stateOver);
        }

        _stateOver = addChild(value) as Container as Container;
        _stateOver.mouseChildren = false;

        // ---

        update();
    }


    // ---

    public function get stateDown():DisplayObject {
        return _stateDown;
    }

    public function set stateDown(value:DisplayObject):void {
        if(_stateDown && _stateDown.parent == this) {
            removeChild(_stateDown);
        }

        _stateDown = addChild(value) as Container;
        _stateDown.mouseChildren = false;

        // ---

        update();
    }


    // ---

    public function get stateDisabled():DisplayObject {
        return _stateDisabled;
    }

    public function set stateDisabled(value:DisplayObject):void {
        if(_stateDisabled && _stateDisabled.parent == this) {
            removeChild(_stateDisabled);
        }

        _stateDisabled = addChild(value) as Container;
        _stateDisabled.mouseChildren = false;

        // ---

        update();
    }


    // ---

    public function get stateChecked():DisplayObject {
        return _stateChecked;
    }

    public function set stateChecked(value:DisplayObject):void {
        if(_stateChecked && _stateChecked.parent == this) {
            removeChild(_stateChecked);
        }

        _stateChecked = addChild(value) as Container;
        _stateChecked.mouseChildren = false;

        // ---

        update();
    }

    // --- Действия для текстовых полей

    public function get text():String {
        return _text;
    }

    public function set text(value:String):void {
        _text = value;

        SpecialUtil.foreach(labels, function(key:String, label:TextLabel):void {
            label.text = value;
        });

        update();
    }

    public function get icon():Class {
        return _icon;
    }

    public function set icon(value:Class):void {
        _icon = value;

        SpecialUtil.foreach(icons, function(key:String, iconContainer:AutoContainer):void {
            iconContainer.removeAllChildren();
            iconContainer.addChild(new value);
        });

        update();
    }

    public function get wordWrap():Boolean {
        return _wordWrap;
    }

    public function set wordWrap(value:Boolean):void {
        _wordWrap = value;

        SpecialUtil.foreach(labels, function(key:String, label:TextLabel):void {
            label.wordWrap = value;
        });

        update();
    }

    public function get bold():Object {
        return _bold;
    }

    public function set bold(value:Object):void {
        _bold = value;

        SpecialUtil.foreach(labels, function(key:String, label:TextLabel):void {
            label.bold = value;
        });

        update();
    }

    // ---

    public function get size():Object {
        return _size;
    }

    public function set size(value:Object):void {
        _size = value;

        SpecialUtil.foreach(labels, function(key:String, label:TextLabel):void {
            label.size = value;
        });

        update();
    }

    // ---

    public function get underline():Object {
        return _underline;
    }

    public function set underline(value:Object):void {
        _underline = value;

        SpecialUtil.foreach(labels, function(key:String, label:TextLabel):void {
            label.underline = value;
        });

        update();
    }

    // ---

    public function get font():String {
        return _font;
    }

    public function set font(value:String):void {
        _font = value;

        SpecialUtil.foreach(labels, function(key:String, label:TextLabel):void {
            label.font = value;
        });

        update();
    }

    // ---

    public function get color():Object {
        return _color;
    }

    public function set color(value:Object):void {
        _color = value;

        SpecialUtil.foreach(labels, function(key:String, label:TextLabel):void {
            label.color = value;
        });

        update();
    }

    // ---

    public function get labelOffsetX():int {
        return _labelOffsetX;
    }

    public function set labelOffsetX(value:int):void {
        _labelOffsetX = value;

        SpecialUtil.foreach(labels, function(key:String, label:TextLabel):void {
            label.offsetX = value;
        });

        update();
    }

    // ---

    public function get labelOffsetY():int {
        return _labelOffsetY;
    }

    public function set labelOffsetY(value:int):void {
        _labelOffsetY = value;

        update();

        SpecialUtil.foreach(labels, function(key:String, label:TextLabel):void {
            label.offsetY = value;
        });
    }

    // ---

    public function get iconOffsetX():int {
        return _iconOffsetX;
    }

    public function set iconOffsetX(value:int):void {
        _iconOffsetX = value;

        update();
    }

    // ---

    public function get iconOffsetY():int {
        return _iconOffsetY;
    }

    public function set iconOffsetY(value:int):void {
        _iconOffsetY = value;

        update();
    }

    // ---

    private function update():void {
        if(created) {
            SpecialUtil.foreach(icons, function(key:String, iconContainer:AutoContainer):void {
                if(_iconAlign) {
                    AlignUtil.align(iconContainer, contentRect, _iconAlign);

                    iconContainer.x += _iconOffsetX;
                    iconContainer.y += _iconOffsetY;
                }

                iconContainer.visible = false;
            });

            SpecialUtil.foreach(labels, function(key:String, label:TextLabel):void {
                if(_textAlign) {
                    AlignUtil.align(label, contentRect, _textAlign);
                }

                label.visible = false;
            });

            if(_stateUp) {
                _stateUp.visible = false;
            }

            if(_stateOver) {
                _stateOver.visible = false;
            }

            if(_stateDown) {
                _stateDown.visible = false;
            }

            if(_stateDisabled) {
                _stateDisabled.visible = false;
            }

            if(_stateChecked) {
                _stateChecked.visible = false;
            }

            switch(_state) {
                case STATE_UP: {
                    if(_stateUp) {
                        _stateUp.visible = true;
                        _iconUp.visible = true;
                        _labelUp.visible = true;
                    }
                    break;
                }

                case STATE_OVER: {
                    if(_stateOver) {
                        _stateOver.visible = true;
                        _iconOver.visible = true;
                        _labelOver.visible = true;
                    }
                    break;
                }

                case STATE_DOWN: {
                    if(_stateDown) {
                        _stateDown.visible = true;
                        _iconDown.visible = true;
                        _labelDown.visible = true;
                    }
                    break;
                }

                case STATE_DISABLED: {
                    if(_stateDisabled) {
                        _stateDisabled.visible = true;
                        _iconDisabled.visible = true;
                        _labelDisabled.visible = true;
                    }
                    break;
                }

                case STATE_CHECKED: {
                    if(_stateChecked) {
                        _stateChecked.visible = true;
                        _iconChecked.visible = true;
                        _labelChecked.visible = true;
                    }
                    break;
                }

                default: {
                    break;
                }
            }

            // ---

            setChildIndex(_labelUp, numChildren - 1);
            setChildIndex(_iconUp, numChildren - 1);

            setChildIndex(_labelOver, numChildren - 1);
            setChildIndex(_iconOver, numChildren - 1);

            setChildIndex(_labelDown, numChildren - 1);
            setChildIndex(_iconDown, numChildren - 1);

            setChildIndex(_labelDisabled, numChildren - 1);
            setChildIndex(_iconDisabled, numChildren - 1);

            setChildIndex(_labelChecked, numChildren - 1);
            setChildIndex(_iconChecked, numChildren - 1);
        }
    }

    // ---

    private function stage_MouseUp_Handler(event:MouseEvent):void {
        _hasMouseDown = false;
    }

    // ---

    private function mouseDown_Handler(event:MouseEvent):void {
        _hasMouseDown = true;

        if(state == STATE_OVER && !_checked) {
            state = STATE_DOWN;
        }
    }

    private function mouseUp_Handler(event:MouseEvent):void {
        if(state == STATE_DOWN && !_checked) {
            if(_hasMouseDown) {
                state = STATE_OVER;
            } else {
                state = STATE_UP;
            }
        }

        _hasMouseDown = false;
    }

    private function mouseOver_Handler(event:MouseEvent):void {
        _hasMouseOver = true;

        if(state == STATE_UP && !_checked) {
            if(_hasMouseDown) {
                state = STATE_DOWN;
            } else {
                state = STATE_OVER;
            }
        }
    }

    private function mouseOut_Handler(event:MouseEvent):void {
        _hasMouseOver = false;

        if((state == STATE_OVER || state == STATE_DOWN) && !_checked) {
            state = STATE_UP;
        }
    }

    private function click_Handler(event:MouseEvent):void {

    }

}
}