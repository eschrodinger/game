package classes {
import classes.containers.Container;
import classes.containers.GradientContainer;
import classes.geometry.Size;

import flash.text.TextField;
import flash.text.TextFieldAutoSize;
import flash.text.TextFormat;

public class TextLabel extends Container {

    private var _text:String;

    private var _debug:Boolean = false;

    private var _color:Object;

    private var _maxContentWidth:int;

    private var _minContentWidth:int;
    private var _minContentHeight:int;

    private var _maxLines:int = 1;

    private var _truncate:Boolean = false;

    private var _suffix:String = "...";

    private var _offsetX:int;
    private var _offsetY:int;

    // ---

    private var textField:TextField;
    private var textFormat:TextFormat;

    private var textMask:GradientContainer;

    public function TextLabel(font:String = "HNCN") {
        super(0, 0, false);

        mouseChildren = false;

        cacheAsBitmap = true;

        // ---

        textFormat = new TextFormat;
        textFormat.font = font;
        textFormat.size = 14;

        // ---

        textField = addChild(new TextField) as TextField;
        textField.x = 0;
        textField.y = 0;
        textField.focusRect = false;
        textField.embedFonts = true;
        textField.selectable = false;
        textField.mouseEnabled = false;
        textField.multiline = true;
        textField.autoSize = TextFieldAutoSize.LEFT;
        textField.defaultTextFormat = textFormat;

        // ---

        update();
    }

    // ---

    override public function debug(value:Boolean, debugColor:int=0xff0000, debugAlpha:Number=0.5):void {
        textField.border = value;
        textField.borderColor = debugColor;
    }

    override public function set contentWidth(value:Number):void {
        super.contentWidth = value;

        _minContentWidth = value;

        update();
    }

    override public function set contentHeight(value:Number):void {
        super.contentHeight = value;

        _minContentHeight = value;

        update();
    }

    override public function set contentSize(value:Size):void {
        super.contentSize = value;

        _minContentWidth = value.width;
        _minContentHeight = value.height;

        update();
    }

    // ---

    public function get minContentHeight():int {
        return _minContentHeight;
    }

    public function set minContentHeight(value:int):void {
        _minContentHeight = value;

        update();
    }

    public function get minContentWidth():int {
        return _minContentWidth;
    }

    public function set minContentWidth(value:int):void {
        _minContentWidth = value;

        update();
    }

    public function get offsetX():int {
        return _offsetX;
    }

    public function set offsetX(value:int):void {
        _offsetX = value;

        update();
    }

    // ---

    public function get offsetY():int {
        return _offsetY;
    }

    public function set offsetY(value:int):void {
        _offsetY = value;

        update();
    }

    // ---

    public function get suffix():String {
        return _suffix;
    }

    public function set suffix(value:String):void {
        _suffix = value;

        update();
    }

    public function get truncate():Boolean {
        return _truncate;
    }

    public function set truncate(value:Boolean):void {
        _truncate = value;

        update();
    }

    public function get maxLines():int {
        return _maxLines;
    }

    public function set maxLines(value:int):void {
        _maxLines = Math.max(value, 0);

        update();
    }

    public function get maxContentWidth():int {
        return _maxContentWidth;
    }

    public function set maxContentWidth(value:int):void {
        _maxContentWidth = value;

        update();
    }

    // ---

    public function set text(value:String):void {
        textField.text = _text = value

        update();
    }

    public function get text():String {
        return _text;
    }

    // ---

    public function set embedFonts(value:Boolean):void {
        textField.embedFonts = value

        update();
    }

    public function get embedFonts():Boolean {
        return textField.embedFonts;
    }

    // ---

    public function set textAlign(value:String):void {
        textFormat.align = value

        update();
    }

    public function get textAlign():String {
        return textFormat.align;
    }

    // ---

    public function set color(value:Object):void {
        _color = value;

        if(textMask && textMask.parent) {
            (removeChild(textMask) as GradientContainer).mask = null;

            addChild(textField);
        }

        if(value is Array) {
            textField.x = 0;
            textField.y = 0;

            textMask = addChild(new GradientContainer(contentWidth, contentHeight, value)) as GradientContainer;
            textMask.mask = textField;
            textMask.mouseEnabled = false;
            textMask.mouseChildren = false;
            textMask.addChild(textField);
        } else {
            textFormat.color = value;
        }

        // ---

        update();
    }

    public function get color():Object {
        return _color;
    }

    // ---

    public function set font(value:String):void {
        textFormat.font = value;

        update();
    }

    public function get font():String {
        return textFormat.font;
    }

    // ---

    public function set underline(value:Object):void {
        textFormat.underline = value;

        update();
    }

    public function get underline():Object {
        return textFormat.underline;
    }

    // ---

    public function set size(value:Object):void {
        textFormat.size = value;

        update();
    }

    public function get size():Object {
        return textFormat.size;
    }

    // ---

    public function set bold(value:Object):void {
        textFormat.bold = value;

        update();
    }

    public function get bold():Object {
        return textFormat.bold;
    }

    // ---

    public function set wordWrap(value:Boolean):void {
        textField.wordWrap = value;

        update();
    }

    public function get wordWrap():Boolean {
        return textField.wordWrap;
    }

    // ---

    public function set htmlText(value:String):void {
        textField.htmlText = value;

        update();
    }

    public function get htmlText():String {
        return textField.htmlText;
    }

    public function clear():void {
        textField.text = "";
    }

    // ---

    private function update():void {
        if(created) {
            textField.defaultTextFormat = textFormat;

            updateTextOptions();
        }
    }

    private function updateTextOptions():void {
        if(created) {
            textField.text = (text || "");

            if(maxLines == 0) {
                textField.wordWrap = true;

                textField.width = maxContentWidth;
            } else if(maxLines == 1) {
                textField.wordWrap = false;

                if(maxContentWidth && textField.width > maxContentWidth) {
                    while(textField.width > maxContentWidth) {
                        textField.text = textField.text.substr(0, (textField.text.length - 1));
                    }

                    textField.text = textField.text.substr(0, (textField.text.length - suffix.length - 1)) + suffix;
                }
            } else {

                textField.wordWrap = true;

                textField.width = maxContentWidth;

                if(textField.numLines > maxLines) {
                    while(textField.numLines > maxLines) {
                        textField.text = textField.text.substr(0, (textField.text.length - 1));
                    }

                    textField.text = textField.text.substr(0, (textField.text.length - suffix.length - 1)) + suffix;
                }
            }

            // ---

            super.contentWidth = Math.max(_minContentWidth, textField.width);
            super.contentHeight = Math.max(_minContentHeight, textField.height);

            // ---

            if(textMask && textMask.parent && _color is Array) {
                textMask.contentSize = new Size(contentWidth, contentHeight);

                textMask.mask = textField;
                textMask.addChild(textField);

                textField.x = 0;

                textMask.x = int((contentWidth - textField.width) / 2 + _offsetX);
            } else {
                textField.x = int((contentWidth - textField.width) / 2 + _offsetX);
            }

            textField.y = int((contentHeight - textField.height) / 2 + _offsetY);
        }

    }

}
}