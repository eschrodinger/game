package classes.utils {
public class ArrayUtil {

    public static function removeItemAt(tarArray:Array, index:int):int {
        if(tarArray.length < index && index > 0) {
            return removeItem(tarArray, tarArray[index]);
        }

        return -1;
    }

    /**
     *
     * @param inArray
     * @param keyValues
     * @return
     *
     */
    public static function getItemByKeys(inArray:Array, keyValues:Object):* {
        var i:int = -1;
        var item:*;
        var hasKeys:Boolean;

        while (++i < inArray.length) {
            item    = inArray[i];
            hasKeys = true;

            for (var j:String in keyValues)
                if (!item.hasOwnProperty(j) || item[j] != keyValues[j])
                    hasKeys = false;

            if (hasKeys)
                return item;
        }

        return null;
    }

    /**
     *
     * @param inArray
     * @param keyValues
     * @return
     *
     */
    public static function getItemsByKeys(inArray:Array, keyValues:Object):Array {
        var t:Array = new Array();
        var i:int   = -1;
        var item:*;
        var hasKeys:Boolean;

        while (++i < inArray.length) {
            item    = inArray[i];
            hasKeys = true;

            for (var j:String in keyValues)
                if (!item.hasOwnProperty(j) || item[j] != keyValues[j])
                    hasKeys = false;

            if (hasKeys)
                t.push(item);
        }

        return t;
    }

    /**
     *
     * @param inArray
     * @param keyValues
     * @return
     *
     */
    public static function getItemByAnyKey(inArray:Array, keyValues:Object):* {
        var i:int = -1;
        var item:*;

        while (++i < inArray.length) {
            item = inArray[i];

            for (var j:String in keyValues)
                if (item.hasOwnProperty(j) && item[j] == keyValues[j])
                    return item;
        }

        return null;
    }

    /**
     *
     * @param inArray
     * @param keyValues
     * @return
     *
     */
    public static function getItemsByAnyKey(inArray:Array, keyValues:Object):Array {
        var t:Array = new Array();
        var i:int   = -1;
        var item:*;
        var hasKeys:Boolean;

        while (++i < inArray.length) {
            item    = inArray[i];
            hasKeys = true;

            for (var j:String in keyValues) {
                if (item.hasOwnProperty(j) && item[j] == keyValues[j]) {
                    t.push(item);

                    break;
                }
            }
        }

        return t;
    }

    /**
     *
     * @param inArray
     * @param key
     * @param match
     * @return
     *
     */
    public static function getItemByKey(inArray:Array, key:String, match:*):* {
        for each (var item:* in inArray)
            if (item.hasOwnProperty(key))
                if (item[key] == match)
                    return item;

        return null;
    }

    /**
     *
     * @param inArray
     * @param key
     * @param match
     * @return
     *
     */
    public static function getItemsByKey(inArray:Array, key:String, match:*):Array {
        var t:Array = new Array();

        for each (var item:* in inArray)
            if (item.hasOwnProperty(key))
                if (item[key] == match)
                    t.push(item);

        return t;
    }

    /**
     *
     * @param inArray
     * @param type
     * @return
     *
     */
    public static function getItemByType(inArray:Array, type:Class):* {
        for each (var item:* in inArray)
            if (item is type)
                return item;

        return null;
    }

    /**
     *
     * @param inArray
     * @param type
     * @return
     *
     */
    public static function getItemsByType(inArray:Array, type:Class):Array {
        var t:Array = new Array();

        for each (var item:* in inArray)
            if (item is type)
                t.push(item);

        return t;
    }

    /**
     *
     * @param inArray
     * @param key
     * @return
     *
     */
    public static function getValuesByKey(inArray:Array, key:String):Array {
        var k:Array = new Array();

        for each (var item:* in inArray)
            if (item.hasOwnProperty(key))
                k.push(item[key]);

        return k;
    }

    /**
     *
     * @param first
     * @param second
     * @return
     *
     */
    public static function equals(first:Array, second:Array):Boolean {
        var i:uint = first.length;
        if (i != second.length)
            return false;

        while (i--)
            if (first[i] != second[i])
                return false;

        return true;
    }

    /**
     *
     * @param tarArray
     * @param items
     * @param index
     * @return
     *
     */
    public static function addItemsAt(tarArray:Array, items:Array, index:int = 0x7FFFFFFF):Boolean {
        if (items.length == 0)
            return false;

        var args:Array = items.concat();
        args.splice(0, 0, index, 0);

        tarArray.splice.apply(null, args);

        return true;
    }

    /**
     *
     * @param inArray
     * @return
     *
     */
    public static function removeDuplicates(inArray:Array):Array {
        return inArray.filter(ArrayUtil._removeDuplicatesFilter);
    }

    protected static function _removeDuplicatesFilter(e:*, i:int, inArray:Array):Boolean {
        return (i == 0) ? true : inArray.lastIndexOf(e, i - 1) == -1;
    }

    /**
     *
     * @param tarArray
     * @param item
     * @return
     *
     */
    public static function removeItem(tarArray:Array, item:*):uint {
        var i:int  = tarArray.indexOf(item);
        var f:uint = 0;

        while (i != -1) {
            tarArray.splice(i, 1);

            i = tarArray.indexOf(item, i);

            f++;
        }

        return f;
    }

    /**
     *
     * @param tarArray
     * @param items
     * @return
     *
     */
    public static function removeItems(tarArray:Array, items:Array):Boolean {
        var removed:Boolean = false;
        var l:uint = tarArray.length;

        while (l--) {
            if (items.indexOf(tarArray[l]) > -1) {
                tarArray.splice(l, 1);
                removed = true;
            }
        }

        return removed;
    }

    /**
     *
     * @param tarArray
     * @param items
     * @return
     *
     */
    public static function retainItems(tarArray:Array, items:Array):Boolean {
        var removed:Boolean = false;
        var l:uint          = tarArray.length;

        while (l--) {
            if (items.indexOf(tarArray[l]) == -1) {
                tarArray.splice(l, 1);
                removed = true;
            }
        }

        return removed;
    }

    /**
     *
     * @param inArray
     * @param item
     * @return
     *
     */
    public static function contains(inArray:Array, item:*):uint {
        var i:int  = inArray.indexOf(item, 0);
        var t:uint = 0;

        while (i != -1) {
            i = inArray.indexOf(item, i + 1);
            t++;
        }

        return t;
    }

    /**
     *
     * @param inArray
     * @param items
     * @return
     *
     */
    public static function containsAll(inArray:Array, items:Array):Boolean {
        var l:uint = items.length;

        while (l--)
            if (inArray.indexOf(items[l]) == -1)
                return false;

        return true;
    }

    /**
     *
     * @param inArray
     * @param items
     * @return
     *
     */
    public static function containsAny(inArray:Array, items:Array):Boolean {
        var l:uint = items.length;

        while (l--)
            if (inArray.indexOf(items[l]) > -1)
                return true;

        return false;
    }

    /**
     *
     * @param first
     * @param second
     * @param fromIndex
     * @return
     *
     */
    public static function getIndexOfDifference(first:Array, second:Array, fromIndex:uint = 0):int {
        var i:int = fromIndex - 1;

        while (++i < first.length)
            if (first[i] != second[i])
                return i;

        return -1;
    }

    /**
     *
     * @param inArray
     * @return
     *
     */
    public static function random(inArray:Array):* {
        return ArrayUtil.randomize(inArray)[0];
    }

    /**
     *
     * @param inArray
     * @return
     *
     */
    public static function randomize(inArray:Array):Array {
        var t:Array = new Array();
        var r:Array = inArray.sort(ArrayUtil._sortRandom, Array.RETURNINDEXEDARRAY);
        var i:int   = -1;

        while (++i < inArray.length)
            t.push(inArray[r[i]]);

        return t;
    }

    protected static function _sortRandom(a:*, b:*):int {
        return NumberUtil.random(0, 1) ? 1 : -1;
    }

    /**
     *
     * @param inArray
     * @return
     *
     */
    public static function sum(inArray:Array):Number {
        var t:Number = 0;
        var l:uint   = inArray.length;

        while (l--)
            t += inArray[l];

        return t;
    }

    /**
     *
     * @param inArray
     * @return
     *
     */
    public static function average(inArray:Array):Number {
        if (inArray.length == 0)
            return 0;

        return ArrayUtil.sum(inArray) / inArray.length;
    }

    /**
     *
     * @param inArray
     * @return
     *
     */
    public static function getLowestValue(inArray:Array):Number {
        return inArray[inArray.sort(16|8)[0]];
    }

    /**
     *
     * @param inArray
     * @return
     *
     */
    public static function getHighestValue(inArray:Array):Number {
        return inArray[inArray.sort(16|8)[inArray.length - 1]];
    }
}
}