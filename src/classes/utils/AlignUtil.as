package classes.utils {
import classes.containers.Container;

import flash.display.DisplayObject;
import flash.geom.Rectangle;

public class AlignUtil {
    public static const TOP:String = "ALIGN_TOP";
    public static const BOTTOM:String = "ALIGN_BOTTOM";
    public static const LEFT:String = "ALIGN_LEFT";
    public static const RIGHT:String = "ALIGN_RIGHT";

    public static const LEFT_TOP:String = "ALIGN_LEFT_TOP";
    public static const RIGHT_TOP:String = "ALIGN_RIGHT_TOP";

    public static const LEFT_BOTTOM:String = "ALIGN_LEFT_BOTTOM";
    public static const RIGHT_BOTTOM:String = "ALIGN_RIGHT_BOTTOM";

    public static const CENTER:String = "ALIGN_CENTER";
    public static const CENTER_TOP:String = "ALIGN_CENTER_TOP";
    public static const CENTER_BOTTOM:String = "ALIGN_CENTER_BOTTOM";

    public static const MIDDLE:String = "ALIGN_MIDDLE";
    public static const MIDDLE_LEFT:String = "ALIGN_MIDDLE_LEFT";
    public static const MIDDLE_RIGHT:String = "ALIGN_MIDDLE_RIGHT";

    public static const MIDDLE_CENTER:String = "ALIGN_MIDDLE_CENTER";

    // ---

    private static var aligns:Array = [
        TOP, BOTTOM, LEFT, RIGHT, LEFT_TOP, RIGHT_TOP,
        LEFT_BOTTOM, RIGHT_BOTTOM, CENTER, CENTER_TOP, CENTER_BOTTOM,
        MIDDLE, MIDDLE_LEFT, MIDDLE_RIGHT, MIDDLE_CENTER
    ];

    public function AlignUtil() {
    }

    public static function isValidAlign(value:String):Boolean {
        var i:int;

        for(i = 0; i < aligns.length; i++) {
            if(aligns[i] == value) {
                return true;
            }
        }

        return false;
    }

    public static function align(container:DisplayObject, rect:Rectangle, type:String):DisplayObject {
        if(!container || !rect) {
            return null;
        }

        var width:int = (container is Container) ? (container as Container).contentWidth : container.width;
        var height:int = (container is Container) ? (container as Container).contentHeight : container.height;

        var rectWidth:int = rect.width;
        var rectHeight:int = rect.height;

        switch(type) {
            case TOP: {
                container.y = rect.y;
                break;
            }

            case BOTTOM: {
                container.y = rectHeight - height;
                break;
            }

            case LEFT: {
                container.x = rect.x;
                break;
            }

            case RIGHT: {
                container.x = rectWidth - width;
                break;
            }

            case LEFT_TOP: {
                container.x = rect.x;
                container.y = rect.y;
                break;
            }

            case RIGHT_TOP: {
                container.x = rectWidth - width;
                container.y = rect.y;
                break;
            }

            case LEFT_BOTTOM: {
                container.x = rect.x;
                container.y = rectHeight - height;
                break;
            }

            case RIGHT_BOTTOM: {
                container.x = rectWidth - width;
                container.y = rectHeight - height;
                break;
            }

            case CENTER: {
                container.x = int((rectWidth - width) / 2);
                break;
            }

            case CENTER_TOP: {
                container.x = int((rectWidth - width) / 2);
                container.y = rect.y;
                break;
            }

            case CENTER_BOTTOM: {
                container.x = int((rectWidth - width) / 2);
                container.y = rectHeight - height;
                break;
            }

            case MIDDLE: {
                container.y = int((rectHeight - height) / 2);
                break;
            }

            case MIDDLE_LEFT: {
                container.x = rect.x;
                container.y = int((rectHeight - height) / 2);
                break;
            }

            case MIDDLE_RIGHT: {
                container.x = rectWidth - width;
                container.y = int((rectHeight - height) / 2);
                break;
            }

            case MIDDLE_CENTER: {
                container.x = int((rectWidth - width) / 2);
                container.y = int((rectHeight - height) / 2);
                break;
            }

            default: {
                break;
            }
        }

        return container;
    }

    public static function getValue(value:Object, childValue:Number, ownerValue:Number):Number {
        if(NumberUtil.check(value)) {
            return value as Number;
        } else if(value is String) {
            if(StringUtil.startsWith(value as String, "%")) {
                return (ownerValue - childValue) / 100.0  * parseFloat((value as String).replace("%", ""));
            } else if(StringUtil.endsWith(value as String, "%")) {
                return (ownerValue / 100.0) * parseFloat((value as String).replace("%", ""));
            }
        }

        return 0;
    }

}
} 