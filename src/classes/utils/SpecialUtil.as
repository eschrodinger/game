package classes.utils {
import classes.containers.Container;
import classes.geometry.Size;

import flash.display.BitmapData;
import flash.display.DisplayObject;
import flash.display.Sprite;
import flash.geom.Matrix;

public class SpecialUtil {
    public function SpecialUtil() {
    }

    public static function doubleClickEnabled(target:Sprite, value:Boolean):void {
        var count:int = target.numChildren;
        var i:int;
        var child:DisplayObject;

        for(i = 0; i < count; i++) {
            child = target.getChildAt(i);
            if(child.hasOwnProperty("doubleClickEnabled")) {
                (child as Sprite).doubleClickEnabled = value;
            }
        }

        target.doubleClickEnabled = value;
    }

    /** Композиция слова и окончани[е, я, ий]
     *
     * @param value - число
     * @param result - слово без окончания
     * @param arg1
     * @param arg2
     * @param arg3
     * @return
     *
     */
    public static function composition(value:int, result:String, arg1:String = "", arg2:String = "", arg3:String = ""):String {
        var count:int = value;

        var last_digit:int = count % 10;
        var last_two_digits:int = count % 100;

        if (last_digit == 1 && last_two_digits != 11) {
            result += arg1;
        } else if ((last_digit == 2 && last_two_digits != 12)
                || (last_digit == 3 && last_two_digits != 13)
                || (last_digit == 4 && last_two_digits != 14)) {
            result += arg2;
        } else {
            result += arg3;
        }

        return result;
    }


    /** Поввернуть BitmapData
     *
     * @param bitmapData
     * @param angle
     * @return
     */
    public static function rotateBitmapData(bitmapData:BitmapData, angle:Number):BitmapData {
        var newData:BitmapData;

        var matrix:Matrix = new Matrix;
        matrix.translate(- bitmapData.width / 2, - bitmapData.height / 2);
        matrix.rotate(angle * (Math.PI / 180));

        if(angle == 0 || angle == 180) {
            matrix.translate(bitmapData.width / 2, bitmapData.height / 2);
            newData = new BitmapData(bitmapData.width, bitmapData.height, bitmapData.transparent);
        } else {
            matrix.translate(bitmapData.height / 2, bitmapData.width / 2);
            newData = new BitmapData(bitmapData.height, bitmapData.width, bitmapData.transparent);
        }

        newData.draw(bitmapData, matrix, null, null, null, true);

        return newData;
    }

    /** Аналог функции php - принимает <b>Array, String, Number, int, uint, Boolean</b>
     *
     * @param variable
     * @return
     */
    public static function empty(variable:*):Boolean {

        var isEmpty:Boolean;

        if (!variable || variable == null || variable == undefined) {
            isEmpty = true;
        } else if (variable is String) {
            var str:String = StringUtil.trim(variable);
            isEmpty = (variable == "" || variable == "0" || variable == "false" || variable == "null");
        } else if (variable is Number || variable is int || variable is uint) {
            isEmpty = (variable == 0);
        } else if (variable is Array) {
            isEmpty = (variable.length == 0);
        } else if (variable is Boolean) {
            isEmpty = (variable == false);
        }

        return isEmpty;
    }

    /** Пропорциональное изменение размеров
     *
     * @param from - исходный размер
     * @param to - максимальный размер
     * @return
     *
     */
    public static function proportion(from:Size, to:Size, maximize:Boolean = true):Size {
        var k:Number = Math.max(from.width / to.width, from.height / to.height);
        return (maximize || from.width > to.width || from.height > to.height) ? new Size(from.width / k, from.height / k) : from;
    }

    /** Пропорционально уменьшает объект
     *
     * @param target  - элемент для изменения размера
     * @param to - пределы нового размера
     * @param maximize - максимизировать или нет (если меньше чем пределы растягивается до них)
     * @return
     *
     */
    public static function autoFitProportion(target:DisplayObject, to:Size, maximize:Boolean = true):DisplayObject {
        var width:int = (target is Container) ? (target as Container).contentWidth : target.width;
        var height:int = (target is Container) ? (target as Container).contentHeight : target.height;

        var result:Size = proportion(new Size(width, height), to, maximize);

        if(target is Container) {
            (target as Container).contentSize = result;
        } else {
            target.width = result.width;
            target.height = result.height;
        }

        return target;
    }

    /** Перебор элементов объекта
     *
     * @param object
     * @param callback - function(key, element)
     *
     */
    public static function foreach(object:Object, callback:Function):void {
        var key:*;

        if(object is Object || object is Array) {
            for (key in object) {
                callback(key, object[key]);
            }
        } else if(object is String) {
            key = 0;

            while(key++ < (object as String).length) {
                callback(key, (object as String).charAt(key));
            }
        } else if(object is int || object is uint) {
            var num:uint = uint(object);
            var i:uint;

            if(num > 0) {
                for(i = num; i >= 0; i--) {
                    callback(i, i);
                }
            } else {
                for(i = num; i <= 0; i++) {
                    callback(i, i);
                }
            }
        }
    }

    /** valueToChunks(10, 3) вернет [3, 3, 3, 1]
     *
     * @param value
     * @param size
     * @return
     */
    public static function valueToChunks(value:int, size:int):Array {
        var chunks:Array = [];

        if(value > size) {
            var pages:int = int(value / size);
            var rest:int = value % size;
            var i:int;

            for(i = 0; i < pages; i++) {
                chunks.push(size);
            }

            if(rest > 0) {
                chunks.push(rest);
            }
        } else {
            chunks = [value];
        }

        return chunks;
    }

    /** valueToArray(10, 3) вернет [4, 3, 3]
     *
     * @param value
     * @param size
     * @return
     */
    public static function valueToArray(value:int, size:int):Array {
        var chunks:Array = [];
        var step:int = value / size;
        var rest:int = value % size;
        var i:int;

        chunks.length = size;

        for(i = 0; i < size; i++) {
            chunks[i] = step + (rest > 0 ? 1 : 0);

            rest --;
        }

        return chunks;
    }
}
}