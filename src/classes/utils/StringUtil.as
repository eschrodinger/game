package classes.utils {
	public class StringUtil {
		public function StringUtil() {
		}
		 
		/** Начинается ли строка <b>string</b> на <b>end</b>
		 * 
		 * @param string
		 * @param start
		 * @return 
		 * 
		 */		
		public static function startsWith(string:String, start:String):Boolean {
			return string.indexOf(start) == 0;
		}
		
		/** Заканчивается ли строка <b>string</b> на <b>end</b>
		 * 
		 * @param string
		 * @param end
		 * @return 
		 * 
		 */		
		public static function endsWith(string:String, end:String):Boolean {
			return string.lastIndexOf(end) == string.length - end.length;
		}
		
		/** Обрезать пробельные смимволы в начале и конце текста
		 * 
		 * @param text
		 * @return 
		 * 
		 */		
		public static function trim(text:String):String {
			return text.replace(/^\s*|\s*$/g, "");
		}
		
	}
}