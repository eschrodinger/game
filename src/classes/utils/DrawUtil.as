package classes.utils {
import flash.display.GradientType;
import flash.display.Graphics;
import flash.geom.Matrix;
import flash.geom.Rectangle;

public class DrawUtil {
    public function DrawUtil() {
    }

    /**
     *
     * @param graphics
     * @param x
     * @param y
     * @param width
     * @param height
     * @param line
     * @param space
     *
     */
    public static function drawDashedLine(graphics:Graphics, x:int, y:int, width:int, height:int, line:int, space:int):void {
        var i:int, count:int;

        if(width) {
            count = int(width / (line + space));

            graphics.moveTo(x, y);

            for(i = 1; i <= count; i++) {
                graphics.lineTo(x + line * i + (space * (i - 1)), y);
                graphics.moveTo(x + line * i + (space * i), y);
            }

            if(width - (count * (line + space))) {
                graphics.lineTo(width, y);
            }

        }

        if(height) {
            count = int(height / (line + space));

            graphics.moveTo(x, y);

            for(i = 1; i <= count; i++) {
                graphics.lineTo(x, y + line * i + (space * (i - 1)));
                graphics.moveTo(x, y + line * i + (space * i));
            }

            if(height - (count * (line + space))) {
                graphics.lineTo(x, height);
            }

        }

    }

    /**
     *
     * @param graphics
     * @param rect
     *
     */
    public static function drawRect(graphics:Graphics, rect:Rectangle):void {
        graphics.drawRect(rect.x, rect.y, rect.width, rect.height);
    }

    /**
     *
     * @param graphics
     * @param x
     * @param y
     * @param width
     * @param height
     * @param ellipseWidth
     * @param ellipseHeight
     * @param topLeft
     * @param topRight
     * @param bottomRight
     * @param bottomLeft
     *
     */
    public static function drawRoundRect(graphics:Graphics, x:Number, y:Number, width:Number, height:Number, ellipseWidth:Number, ellipseHeight:Number, topLeft:Boolean = true, topRight:Boolean = true, bottomRight:Boolean = true, bottomLeft:Boolean = true):void {
        const radiusWidth:Number  = ellipseWidth * 0.5;
        const radiusHeight:Number = ellipseHeight * 0.5;

        if (topLeft) {
            graphics.moveTo(x + radiusWidth, y);
        } else {
            graphics.moveTo(x, y);
        }

        if (topRight) {
            graphics.lineTo(x + width - radiusWidth, y);
            graphics.curveTo(x + width, y, x + width, y + radiusHeight);
        } else {
            graphics.lineTo(x + width, y);
        }

        if (bottomRight) {
            graphics.lineTo(x + width, y + height - radiusHeight);
            graphics.curveTo(x + width, y + height, x + width - radiusWidth, y + height);
        } else {
            graphics.lineTo(x + width, y + height);
        }

        if (bottomLeft) {
            graphics.lineTo(x + radiusWidth, y + height);
            graphics.curveTo(x, y + height, x, y + height - radiusHeight);
        } else {
            graphics.lineTo(x, y + height);
        }

        if (topLeft) {
            graphics.lineTo(x, y + radiusHeight);
            graphics.curveTo(x, y, x + radiusWidth, y);
        } else {
            graphics.lineTo(x, y);
        }

    }

    public static function fillGradiend(graphics:Graphics, width:int, height:int, angle:Number, colors:Array, alphas:Array):void {
        var matrix:Matrix = new Matrix;

        matrix.createGradientBox(width, height, angle);

        graphics.beginGradientFill(GradientType.LINEAR, colors, alphas, [0, 255], matrix);
    }

}
}