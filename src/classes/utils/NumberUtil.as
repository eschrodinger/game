package classes.utils {
public class NumberUtil {
    public function NumberUtil() {
    }

    /** min <= value <= max
     *
     * @param value
     * @param min
     * @param max
     * @return
     *
     */
    public static function limit(value:Number, min:Number, max:Number):Number {
        return Math.max(min, Math.min(value, max));
    }

    /** random
     *
     * @param min
     * @param max
     * @return
     *
     */
    public static function random(min:int = 0, max:int = 1): int {
        return Math.floor(Math.random() * (1 + max)) + min;
    }

    public static function check(value:Object):Boolean {
        return (value is int) || (value is uint) || (value is Number);
    }

}
}