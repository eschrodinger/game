package classes.utils {
import flash.utils.clearInterval;
import flash.utils.setInterval;

public class TimerUtil {
    public function TimerUtil() {
    }

    private static var functions:Object = {};
    private static var intervals:Object = {};

    public static function startInterval(time:int, callback:Function):void {
        functions[time] = functions[time] || [];

        (functions[time] as Array).push(callback);

        if((functions[time] as Array).length == 1) {
            intervals[time] = setInterval(function(key:int):void {
                var lists:Array = functions[key];
                var i:int, count:int = lists.length;

                for(i = 0; i < count; i++) {
                    if(lists[i] is Function) {
                        lists[i]();
                    }
                }

            }, time, time);
        }
    }

    public static function stopInterval(time:int, callback:Function):void {
        var lists:Array = functions[time] || [];
        var index:int = lists.indexOf(callback);

        if(index != -1) {
            ArrayUtil.removeItem(lists, lists[index]);
        }

        if(!lists.length) {
            clearInterval(intervals[time]);

            functions[time] = null;
        }
    }

}
}