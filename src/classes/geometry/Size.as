package classes.geometry {
public class Size extends Object {

    public var width:Number;
    public var height:Number;

    public function Size(width:Number, height:Number) {
        super();

        this.width = width;
        this.height = height;
    }
}
}
