package classes.dialog {
import classes.containers.Container;
import classes.containers.CornerContainer;

import flash.events.MouseEvent;

public class Dialog extends CornerContainer {

    private var _visible:Boolean = false;

    private var _manager:DialogManager;

    private var _modal:Boolean = false;

    private var _closeButton:Container;

    // ---

    public var showEffect:Function = DialogEffects.smoothShowEffect;
    public var hideEffect:Function = DialogEffects.smoothHideEffect;

    public var showEffectDuration:Number = 0.5;
    public var hideEffectDuration:Number = 0.5;


    public function Dialog(_width:int, _height:int, _corner:int) {
        super(_width, _height, _corner);

        // ---

        mouseEnabled = true;

        masked = false;

        // ---

        _closeButton = addChild(new Container(0, 0, false)) as Container;
        _closeButton.mouseEnabled = true;
        _closeButton.useHandCursor = true;
        _closeButton.buttonMode = true;
        _closeButton.mouseChildren = false;
        _closeButton.addEventListener(MouseEvent.CLICK, closeButton_Click_Handler);

        // ---

        update();
    }

    // ---

    override public function set visible(value:Boolean):void {
        if(value) {
            show();
        } else {
            hide();
        }
    }

    override public function get visible():Boolean {
        return _visible;
    }

    // ---

    public function get modal():Boolean {
        return _modal;
    }

    public function set modal(value:Boolean):void {
        _modal = value;
    }

    public function get manager():DialogManager {
        return _manager;
    }

    public function set manager(value:DialogManager):void {
        _manager = value;

        hide();
    }

    public function get useCloseButton():Boolean {
        return _closeButton.visible;
    }

    public function set useCloseButton(value:Boolean):void {
        _closeButton.visible = value;
    }

    public function get closeButton():Container {
        return _closeButton;
    }

    public function show():void {
        if(_visible) {
            return;
        }

        _visible = true;

        dispatchEvent(new DialogEvent(DialogEvent.BEFORE_SHOW));

        showEffect(this, _manager);
    }

    public function hide():void {
        if(!_visible) {
            return;
        }

        _visible = false;

        dispatchEvent(new DialogEvent(DialogEvent.BEFORE_HIDE));

        hideEffect(this, _manager);
    }

    // ---

    private function update():void {

    }

    // ---

    private function closeButton_Click_Handler(event:MouseEvent):void {
        hide();
    }

}
}