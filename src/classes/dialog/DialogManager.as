package classes.dialog {
import classes.containers.Container;
import classes.events.ContainerEvent;
import classes.utils.ArrayUtil;
import classes.utils.SpecialUtil;

import flash.events.MouseEvent;

public class DialogManager extends Container {

    private var _overlay:Class;

    // ---

    private var overlayContainer:Container;

    private var dialogs:Array = [];

    public function DialogManager(_width:int, _height:int, _masked:Boolean) {
        super(_width, _height, _masked);

        // ---

        addEventListener(ContainerEvent.CONTENT_SIZE_CHANGE, contentSizeChange_Handler);

        update();
    }

    // ---

    public function set overlay(value:Class):void {
        if(overlayContainer) {
            overlayContainer.removeEventListener(MouseEvent.CLICK, overlayContainer_MouseClick_Handler);
        }

        _overlay = value;

        overlayContainer = new _overlay(contentWidth, contentHeight);
        overlayContainer.mouseEnabled = true;
        overlayContainer.addEventListener(MouseEvent.CLICK, overlayContainer_MouseClick_Handler);
    }

    public function get overlay():Class {
        return _overlay;
    }

    public function get numDialogs():int {
        return dialogs.length;
    }

    public function addDialog(dialog:Dialog):void {
        addChild(overlayContainer);

        dialogs.push(addChild(dialog));
    }

    public function removeDialog(dialog:Dialog):void {
        if(hasChild(dialog)) {
            ArrayUtil.removeItem(dialogs, removeChild(dialog));

            if(numDialogs) {
                addChildAt(overlayContainer, numChildren - 1);
            } else {
                removeChild(overlayContainer);
            }
        }
    }

    public function hideAll():void {
        SpecialUtil.foreach(dialogs, function(index:int, dialog:Dialog):void {
            dialog.hide();
        });
    }

    // ---

    private function update():void {
        if(overlayContainer) {
            overlayContainer.contentSize = contentSize;
        }
    }

    // ---

    private function overlayContainer_MouseClick_Handler(event:MouseEvent):void {
        if(event.currentTarget == overlayContainer && numDialogs && (dialogs[numDialogs - 1] as Dialog).modal) {
            (dialogs[numDialogs - 1] as Dialog).hide();
        }
    }

    private function contentSizeChange_Handler(event:ContainerEvent):void {
        update();
    }
}
}