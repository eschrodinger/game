package classes.dialog {
import classes.base.BaseEvent;

import flash.events.Event;

public class DialogEvent extends BaseEvent {
		
		public static const BEFORE_SHOW:String = "DialogEvent:BEFORE_SHOW"; 
		public static const BEFORE_HIDE:String = "DialogEvent:BEFORE_HIDE"; 
		
		public static const SHOW:String = "DialogEvent:SHOW"; 
		public static const HIDE:String = "DialogEvent:HIDE"; 
		
		public function DialogEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false) {
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event { 
			var newEvent:DialogEvent = new DialogEvent(type);
			
			return newEvent;
		}
	}
}