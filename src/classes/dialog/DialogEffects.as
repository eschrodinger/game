package classes.dialog {
public class DialogEffects {
    public function DialogEffects() {
    }

    public static function smoothShowEffect(dialog:Dialog, manager:DialogManager):void {
        manager.addDialog(dialog);

        dialog.dispatchEvent(new DialogEvent(DialogEvent.SHOW));
    }

    public static function smoothHideEffect(dialog:Dialog, manager:DialogManager):void {
        manager.removeDialog(dialog);

        dialog.dispatchEvent(new DialogEvent(DialogEvent.HIDE));
    }

}
}