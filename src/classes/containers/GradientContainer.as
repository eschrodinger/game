package classes.containers {
import classes.events.ContainerEvent;
import classes.utils.DrawUtil;

public class GradientContainer extends CornerContainer {

    private var _colors:Object;
    private var _alphas:Array = [1, 1];

    private var _angle:Number;

    public function GradientContainer(_width:int, _height:int, _colors:Object, _angle:Number = Math.PI / 2) {
        super(_width, _height, 0);

        this._colors = _colors;
        this._angle = _angle;

        // ---

        addEventListener(ContainerEvent.CONTENT_FORM_CHANGE, contentSizeChange_Handler);

        update();
    }

    // ---

    public function get alphas():Array {
        return _alphas;
    }

    public function set alphas(value:Array):void 	{
        _alphas = value;

        update();
    }

    // ---

    public function get angle():Number {
        return _angle;
    }

    public function set angle(value:Number):void {
        _angle = value;

        update();
    }

    public function get colors():Object {
        return _colors;
    }

    public function set colors(value:Object):void {
        _colors = value;

        update();
    }

    // ---

    private function update():void {
        if(created) {
            if(_colors && _angle) {
                graphics.clear();

                if(_colors is Array) {
                    DrawUtil.fillGradiend(graphics, contentWidth, contentHeight, _angle, _colors as Array, _alphas);
                } else {
                    graphics.beginFill(_colors as uint);
                }

                DrawUtil.drawRect(graphics, contentRect);

                graphics.endFill();
            }
        }
    }

    // ---

    private function contentSizeChange_Handler(event:ContainerEvent):void {
        update();
    }

}
}