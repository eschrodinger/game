package classes.containers {
import classes.events.ContainerEvent;
import classes.utils.AlignUtil;
import classes.utils.SpecialUtil;

import flash.display.DisplayObject;

public class VGroupContainer extends Container {;

    private var _gap:int;

    private var _hAlign:String = AlignUtil.LEFT;

    private var _minContentWidth:int;
    private var _minContentHeight:int;

    public function VGroupContainer(_masked:Boolean) {
        super(0, 0, _masked);
    }

    // ---

    override public function addChild(child:DisplayObject):DisplayObject {
        var target:DisplayObject = super.addChild(child);

        update();

        return events(target);
    }

    override public function addChildAt(child:DisplayObject, index:int):DisplayObject {
        var target:DisplayObject = super.addChildAt(child, index);

        update();

        return events(target);
    }

    override public function removeChild(child:DisplayObject):DisplayObject {
        var target:DisplayObject = super.removeChild(child);

        update();

        return events(target, true);
    }

    override public function removeChildAt(index:int):DisplayObject {
        var target:DisplayObject = super.removeChildAt(index);

        update();

        return events(target, true);
    }

    override public function setChildIndex(child:DisplayObject, index:int):void {
        super.setChildIndex(child, index);

        update();
    }

    // ---

    public function get gap():int {
        return _gap;
    }

    public function set gap(value:int):void {
        _gap = value;

        update();
    }

    public function get minContentWidth():int {
        return _minContentWidth;
    }

    public function set minContentWidth(value:int):void {
        _minContentWidth = value;

        update();
    }

    public function get minContentHeight():int {
        return _minContentHeight;
    }

    public function set minContentHeight(value:int):void {
        _minContentHeight = value;

        update();
    }

    public function get hAlign():String {
        return _hAlign;
    }

    /** <b>AlignUtil.ALIGN_LEFT</b>, AlignUtil.ALIGN_CENTER, AlignUtil.ALIGN_RIGHT
     *
     * @param value
     *
     */
    public function set hAlign(value:String):void {
        _hAlign = value;

        update();
    }

    public function layout():void {
        update();
    }

    // ---

    private function update():void {
        var maxWidth:int = 0;
        var maxHeight:int = 0;

        var i:int;

        var child:Object;
        var childWidth:int;
        var childHeight:int;

        var lastPosition:int;

        for(i = 0; i < numChildren; i++) {
            child = getChildAt(i) as Object;

            childWidth = (child is Container) ? (child as Container).contentWidth : child.width;
            childHeight = (child is Container) ? (child as Container).contentHeight : child.height;

            maxWidth = Math.max(maxWidth, childWidth);
            maxHeight = Math.max(maxHeight, childHeight);

            child.y = lastPosition;

            lastPosition += childHeight + gap;
        }

        maxWidth = Math.max(minContentWidth, maxWidth);

        if(minContentHeight && minContentHeight > (lastPosition - gap) && numChildren) {
            var chunks:Array = SpecialUtil.valueToArray(minContentHeight - (lastPosition - gap), numChildren - 1);

            lastPosition = 0;

            for(i = 0; i < numChildren; i++) {
                child = getChildAt(i) as Object;

                childWidth = (child is Container) ? (child as Container).contentWidth : child.width;
                childHeight = (child is Container) ? (child as Container).contentHeight : child.height;

                child.y = lastPosition;

                lastPosition += childHeight + gap + (i < chunks.length ? chunks[i] : 0);

            }
        }

        for(i = 0; i < numChildren; i++) {
            child = getChildAt(i) as Object;

            childWidth = (child is Container) ? (child as Container).contentWidth : child.width;
            childHeight = (child is Container) ? (child as Container).contentHeight : child.height;

            switch(_hAlign) {
                case AlignUtil.LEFT: {
                    child.x = 0;
                    break;
                }

                case AlignUtil.CENTER: {
                    child.x = (maxWidth - childWidth) / 2;
                    break;
                }

                case AlignUtil.RIGHT: {
                    child.x = maxWidth - childWidth;
                    break;
                }

                default: {

                    break;
                }
            }

        }

        // ---

        contentHeight = lastPosition - (numChildren > 1 ? gap : 0);
        contentWidth = maxWidth;
    }

    private function events(child:Object, remove:Boolean = false):DisplayObject {
        if(child is Container) {
            if(remove) {
                child.removeEventListener(ContainerEvent.CONTENT_SIZE_CHANGE, child_ContentSizeChange_Handler);
            } else {
                child.addEventListener(ContainerEvent.CONTENT_SIZE_CHANGE, child_ContentSizeChange_Handler);
            }
        }

        return child as DisplayObject;
    }

    // ---

    private function child_ContentSizeChange_Handler(event:ContainerEvent):void {
        update();
    }

}
}