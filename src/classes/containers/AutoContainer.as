package classes.containers {
import classes.events.ContainerEvent;

import flash.display.DisplayObject;

public class AutoContainer extends Container {
    public function AutoContainer(child:DisplayObject = null) {
        super(0, 0, false);

        if(child) {
            addChild(child);
        }

        addEventListener(ContainerEvent.CONTENT_UPDATE, contentUpdate_Handler);

        // ---

        update();
    }

    // ---

    public function layout():void {
        update();
    }

    // ---

    private function update():void {
        var maxWidth:int = 0;
        var maxHeight:int = 0;

        var i:int;

        var child:DisplayObject;
        var childWidth:int;
        var childHeight:int;

        for(i = 0; i < numChildren; i++) {
            child = getChildAt(i);

            childWidth = (child is Container) ? (child as Container).contentWidth : child.width;
            childHeight = (child is Container) ? (child as Container).contentHeight : child.height;

            maxWidth = Math.max(maxWidth, childWidth);
            maxHeight = Math.max(maxHeight, childHeight);
        }

        contentWidth = maxWidth;
        contentHeight = maxHeight;
    }

    // ---

    private function contentUpdate_Handler(event:ContainerEvent):void {
        update();
    }

}
}