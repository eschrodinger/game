package classes.containers {
import classes.events.ContainerEvent;
import classes.utils.AlignUtil;
import classes.utils.SpecialUtil;

import flash.display.DisplayObject;

import helpers.Console;

public class HGroupContainer extends Container {;

    private var _gap:int;

    private var _vAlign:String = AlignUtil.TOP;

    private var _minContentWidth:int;
    private var _minContentHeight:int;

    public function HGroupContainer(_masked:Boolean) {
        super(0, 0, _masked);
    }

    // ---

    override public function addChild(child:DisplayObject):DisplayObject {
        var target:DisplayObject = super.addChild(child);

        update();

        return events(target);
    }

    override public function addChildAt(child:DisplayObject, index:int):DisplayObject {
        var target:DisplayObject = super.addChildAt(child, index);

        update();

        return events(target);
    }

    override public function removeChild(child:DisplayObject):DisplayObject {
        var target:DisplayObject = super.removeChild(child);

        update();

        return events(target, true);
    }

    override public function removeChildAt(index:int):DisplayObject {
        var target:DisplayObject = super.removeChildAt(index);

        update();

        return events(target, true);
    }

    override public function setChildIndex(child:DisplayObject, index:int):void {
        super.setChildIndex(child, index);

        update();
    }

    // ---

    public function get gap():int {
        return _gap;
    }

    public function set gap(value:int):void {
        _gap = value;

        update();
    }

    public function get minContentWidth():int {
        return _minContentWidth;
    }

    public function set minContentWidth(value:int):void {
        _minContentWidth = value;

        update();
    }

    public function get minContentHeight():int {
        return _minContentHeight;
    }

    public function set minContentHeight(value:int):void {
        _minContentHeight = value;

        update();
    }

    public function get vAlign():String {
        return _vAlign;
    }

    /** <b>AlignUtil.ALIGN_TOP</b>, AlignUtil.ALIGN_MIDDLE, AlignUtil.ALIGN_BOTTOM
     *
     * @param value
     *
     */
    public function set vAlign(value:String):void {
        _vAlign = value;

        update();
    }

    public function layout():void {
        update();
    }

    // ---

    private function update():void {
        var maxWidth:int = 0;
        var maxHeight:int = 0;

        var i:int;

        var child:Object;
        var childWidth:int;
        var childHeight:int;

        var lastPosition:int;

        for(i = 0; i < numChildren; i++) {
            child = getChildAt(i) as Object;

            childWidth = (child is Container) ? (child as Container).contentWidth : child.width;
            childHeight = (child is Container) ? (child as Container).contentHeight : child.height;

            maxWidth = Math.max(maxWidth, childWidth);
            maxHeight = Math.max(maxHeight, childHeight);

            child.x = lastPosition;

            lastPosition += childWidth + gap;
        }

        maxHeight = Math.max(minContentHeight, maxHeight);

        if(minContentWidth && minContentWidth > (lastPosition - gap) && numChildren) {
            var chunks:Array = SpecialUtil.valueToArray(minContentWidth - (lastPosition - gap), numChildren - 1);

            lastPosition = 0;

            for(i = 0; i < numChildren; i++) {
                child = getChildAt(i) as Object;

                childWidth = (child is Container) ? (child as Container).contentWidth : child.width;
                childHeight = (child is Container) ? (child as Container).contentHeight : child.height;

                child.x = lastPosition;

                lastPosition += childWidth + gap + (i < chunks.length ? chunks[i] : 0);

            }
        }

        for(i = 0; i < numChildren; i++) {
            child = getChildAt(i) as Object;

            childWidth = (child is Container) ? (child as Container).contentWidth : child.width;
            childHeight = (child is Container) ? (child as Container).contentHeight : child.height;

            switch(_vAlign) {
                case AlignUtil.TOP: {
                    child.y = 0;
                    break;
                }

                case AlignUtil.MIDDLE: {
                    child.y = (maxHeight - childHeight) / 2;
                    break;
                }

                case AlignUtil.BOTTOM: {
                    child.y = maxHeight - childHeight;
                    break;
                }

                default: {

                    break;
                }
            }

        }

        // ---

        contentHeight = maxHeight;
        contentWidth = lastPosition - (numChildren > 1 ? gap : 0);
    }

    private function events(child:Object, remove:Boolean = false):DisplayObject {
        if(child is Container) {
            if(remove) {
                child.removeEventListener(ContainerEvent.CONTENT_SIZE_CHANGE, child_ContentSizeChange_Handler);
            } else {
                child.addEventListener(ContainerEvent.CONTENT_SIZE_CHANGE, child_ContentSizeChange_Handler);
            }
        }

        return child as DisplayObject;
    }

    // ---

    private function child_ContentSizeChange_Handler(event:ContainerEvent):void {
        update();
    }

}
}