package classes.containers {
import classes.events.ContainerEvent;
import classes.geometry.Size;
import classes.utils.AlignUtil;
import classes.utils.ArrayUtil;
import classes.utils.TimerUtil;

import flash.display.DisplayObject;
import flash.display.DisplayObjectContainer;
import flash.display.Graphics;
import flash.display.Shape;
import flash.display.Sprite;
import flash.events.Event;
import flash.geom.Rectangle;

public class Container extends Sprite {

    protected var baseMask:Shape;

    protected var baseContainer:Sprite;

    // ---

    private var _contentWidth:Number = 0;
    private var _contentHeight:Number = 0;
    private var _masked:Boolean;

    // ---

    private var _align:String;
    private var _alignRect:Rectangle;

    private var _left:Object = null;
    private var _right:Object = null;
    private var _top:Object = null;
    private var _bottom:Object = null;

    private var _verticalCenter:Object = null;
    private var _horizontalCenter:Object = null;

    private var _background:Object;
    private var _backgroundAlpha:Number = 1;

    private var _created:Boolean = false;

    // ---

    private var _debug:Boolean = false;
    private var _debugColor:int;
    private var _debugAlpha:Number;

    // ---

    private var _owner:Container;

    // ---


    private var childs:Array = [];
    private var sizes:Array = [];

    public function Container(_width:int, _height:int, _masked:Boolean) {
        super();

        // ---

        tabEnabled = false;
        tabChildren = false;
        mouseEnabled = false;

        // ---

        baseMask = super.addChild(new Shape) as Shape;

        baseContainer = super.addChild(new Sprite) as Sprite;
        baseContainer.mouseEnabled = false;
        baseContainer.cacheAsBitmap = true;

        // ---

        this._masked =  _masked;

        this._contentWidth = _width;
        this._contentHeight = _height;

        // ---

        stage ? ready() : addEventListener(Event.ADDED_TO_STAGE, addedToStage_Handler);

        // ---

        _created = true;

        // ---

        update();

        updateMask();

        // ---

        TimerUtil.startInterval(1000, checkChildsSizes);
    }

    // ---

    override public function set x(value:Number):void {
        super.x = int(value);
    }

    override public function set y(value:Number):void {
        super.y = int(value);
    }

    override public function get parent():DisplayObjectContainer {
        return _owner;
    }

    override public function get graphics():Graphics {
        return baseContainer.graphics;
    }

    override public function get numChildren():int {
        return baseContainer.numChildren;
    }

    override public function get mask():DisplayObject {
        return baseContainer.mask;
    }

    override public function set mask(value:DisplayObject):void {
        baseContainer.mask = value;
    }

    override public function addChild(child:DisplayObject):DisplayObject {
        var target:DisplayObject = baseContainer.addChild(child);

        if(target is Container) {
            (target as Container).owner = this;
        }

        dispatchEvent(new ContainerEvent(ContainerEvent.CONTENT_CHILD_ADDED));
        dispatchEvent(new ContainerEvent(ContainerEvent.CONTENT_CHILDS_COUNT_CHANGE));

        return childEvents(target);
    }

    override public function addChildAt(child:DisplayObject, index:int):DisplayObject {
        var target:DisplayObject = baseContainer.addChildAt(child, index);

        if(target is Container) {
            (target as Container).owner = this;
        }

        dispatchEvent(new ContainerEvent(ContainerEvent.CONTENT_CHILD_ADDED));
        dispatchEvent(new ContainerEvent(ContainerEvent.CONTENT_CHILDS_COUNT_CHANGE));

        return childEvents(target);
    }

    override public function removeChild(child:DisplayObject):DisplayObject {
        var target:DisplayObject = baseContainer.removeChild(child);

        if(target is Container) {
            (target as Container).owner = null;
        }

        dispatchEvent(new ContainerEvent(ContainerEvent.CONTENT_CHILD_REMOVED));
        dispatchEvent(new ContainerEvent(ContainerEvent.CONTENT_CHILDS_COUNT_CHANGE));

        return childEvents(target, true);
    }

    override public function removeChildAt(index:int):DisplayObject {
        var target:DisplayObject = baseContainer.removeChildAt(index);

        if(target is Container) {
            (target as Container).owner = null;
        }

        dispatchEvent(new ContainerEvent(ContainerEvent.CONTENT_CHILD_REMOVED));
        dispatchEvent(new ContainerEvent(ContainerEvent.CONTENT_CHILDS_COUNT_CHANGE));

        return childEvents(target, true);
    }

    override public function setChildIndex(child:DisplayObject, index:int):void {
        baseContainer.setChildIndex(child, index);
    }

    override public function getChildIndex(child:DisplayObject):int {
        return baseContainer.getChildIndex(child);
    }

    override public function getChildAt(index:int):DisplayObject {
        return baseContainer.getChildAt(index);
    }

    override public function getChildByName(name:String):DisplayObject {
        return baseContainer.getChildByName(name);
    }

    // ---

    /**
     *  Отрабатывает при добавлении элемента на stage
     */
    public function initialize():void {

    }

    public function getAllChildren():Array {
        var result:Array = [];
        var i:int = 0;

        for(i = 0; i < numChildren; i++) {
            result.push(getChildAt(i));
        }

        return result;
    }

    public function removeAllChildren():int {
        var count:int = numChildren;

        while(numChildren) {
            removeChildAt(0);
        }

        return count;
    }

    public function hasChild(child:DisplayObject):Boolean {
        return (child is Container) ? (child as Container).owner == this : (child.parent && child.parent == baseContainer);
    }

    public function get created():Boolean {
        return _created;
    }

    public function debug(value:Boolean, debugColor:int = 0xff0000, debugAlpha:Number = 0.5):void {
        _debug = value;

        _debugColor = debugColor;

        _debugAlpha = debugAlpha;

        update();
    }

    public function resize(_width:int, _height:int):void {
        contentSize = new Size(_width, _height);
    }

    /**
     * Ширина контента
     **/
    public function set contentWidth(value:Number):void {
        var oldValue:Number = _contentWidth;

        _contentWidth = value;

        if(oldValue != value) {
            update();

            dispatchEvent(new ContainerEvent(ContainerEvent.CONTENT_WIDTH_CHANGE));

            dispatchEvent(new ContainerEvent(ContainerEvent.CONTENT_SIZE_CHANGE));

            dispatchEvent(new ContainerEvent(ContainerEvent.CONTENT_FORM_CHANGE));
        }
    }

    public function get contentWidth():Number {
        return _contentWidth;
    }

    /**
     * Высота контента
     **/
    public function set contentHeight(value:Number):void {
        var oldValue:Number = _contentHeight;

        _contentHeight = value;

        if(oldValue != value) {
            update();

            dispatchEvent(new ContainerEvent(ContainerEvent.CONTENT_HEIGHT_CHANGE));

            dispatchEvent(new ContainerEvent(ContainerEvent.CONTENT_SIZE_CHANGE));

            dispatchEvent(new ContainerEvent(ContainerEvent.CONTENT_FORM_CHANGE));
        }
    }

    public function get contentHeight():Number {
        return _contentHeight;
    }

    public function get contentSize():Size {
        return new Size(_contentWidth, _contentHeight);
    }

    public function set contentSize(value:Size):void {
        contentWidth = value.width;
        contentHeight = value.height;
    }

    public function get background():Object {
        return _background;
    }

    public function set background(value:Object):void {
        var oldValue:Object = _background;

        _background = value;

        if(oldValue != value) {
            update();
        }
    }

    public function get backgroundAlpha():Number {
        return _backgroundAlpha;
    }

    public function set backgroundAlpha(value:Number):void {
        var oldValue:Number = _backgroundAlpha;

        _backgroundAlpha = value;

        if(oldValue != value) {
            update();
        }
    }

    public function get align():String {
        return _align;
    }

    /** Позиционирование отностительно родителя
     *
     * @param value
     */
    public function set align(value:String):void {
        var oldValue:String = _align;

        _align = value;

        if(oldValue != value) {
            update();
        }
    }

    public function get alignRect():Rectangle {
        return _alignRect;
    }

    public function set alignRect(value:Rectangle):void {
        var oldValue:Rectangle = _alignRect;

        _alignRect = value;

        if(oldValue != value) {
            update();
        }
    }

    public function get left():Object {
        return _left;
    }

    public function set left(value:Object):void {
        var oldValue:Object = _left;

        _left = value;

        if(oldValue != value) {
            update();
        }
    }

    public function get right():Object {
        return _right;
    }

    public function set right(value:Object):void {
        var oldValue:Object = _right;

        _right = value;

        if(oldValue != value) {
            update();
        }
    }

    public function get top():Object {
        return _top;
    }

    public function set top(value:Object):void {
        var oldValue:Object = _top;

        _top = value;

        if(oldValue != value) {
            update();
        }
    }

    public function get bottom():Object {
        return _bottom;
    }

    public function set bottom(value:Object):void {
        var oldValue:Object = _bottom;

        _bottom = value;

        if(oldValue != value) {
            update();
        }
    }

    public function get verticalCenter():Object {
        return _verticalCenter;
    }

    public function set verticalCenter(value:Object):void {
        var oldValue:Object = _verticalCenter;

        _verticalCenter = value;

        if(oldValue != value) {
            update();
        }
    }

    public function get horizontalCenter():Object {
        return _horizontalCenter;
    }

    public function set horizontalCenter(value:Object):void {
        var oldValue:Object = _horizontalCenter;

        _horizontalCenter = value;

        if(oldValue != value) {
            update();
        }
    }

    /** Устанавливает/получаем наличее маски на контенте
     *
     * @param value
     */
    public function set masked(value:Boolean):void {
        var oldValue:Boolean = _masked;

        _masked = value;

        if(oldValue != value) {
            updateMask();
        }
    }

    public function get masked():Boolean {
        return _masked;
    }

    public function set owner(value:Container):void {
        var oldValue:Container = _owner;

        _owner = value;

        if(oldValue != value) {
            if(oldValue) {
                ownerEvents(oldValue, true);
            }

            if(_owner) {
                ownerEvents(_owner);
            }
        }
    }

    public function get owner():Container {
        return _owner;
    }

    public function get contentRect():Rectangle {
        return new Rectangle(0, 0, contentWidth, contentHeight);
    }

    // ---

    private function ready():void {
        //debug(true, NumberUtil.random(0x000000, 0xffffff));

        initialize();
    }

    private function update():void {
        updateSize();

        updatePosition();

        if(_debug) {
            graphics.beginFill(_debugColor, _debugAlpha);
            graphics.drawRect(0, 0, _contentWidth, _contentHeight);
            graphics.endFill();
        }

        // ---

        dispatchEvent(new ContainerEvent(ContainerEvent.CONTENT_UPDATE));
    }

    private function updateSize():void {
        baseMask.graphics.clear();
        baseMask.graphics.beginFill(0, 0);
        baseMask.graphics.drawRect(0, 0, _contentWidth, _contentHeight);
        baseMask.graphics.endFill();

        // ---

        super.graphics.clear();

        (_background is int || _background is uint) ? super.graphics.beginFill(_background as uint, _backgroundAlpha) : super.graphics.beginFill(0, 0);

        super.graphics.drawRect(0, 0, _contentWidth, _contentHeight);
        super.graphics.endFill();
    }

    private function updatePosition():void {
        var i:int;

        if(_owner) {
            if(AlignUtil.isValidAlign(_align)) {
                AlignUtil.align(this, alignRect ? alignRect : (_owner as Container).contentRect, _align);
            } else if(_verticalCenter != null || _horizontalCenter != null) {

                if(_verticalCenter != null && _horizontalCenter != null) {
                    AlignUtil.align(this, alignRect ? alignRect : (_owner as Container).contentRect, AlignUtil.MIDDLE_CENTER);
                    x += _horizontalCenter as Number;
                    y += _verticalCenter as Number;
                } else if(_verticalCenter != null) {
                    AlignUtil.align(this, alignRect ? alignRect : (_owner as Container).contentRect, AlignUtil.MIDDLE);

                    y += _verticalCenter as Number;

                    checkLeftRight();
                } else if(_horizontalCenter != null) {
                    AlignUtil.align(this, alignRect ? alignRect : (_owner as Container).contentRect, AlignUtil.CENTER);

                    x += _horizontalCenter as Number;

                    checkTopBottom();
                }
            } else if(_left != null || _right != null || _top != null || _bottom != null) {
                checkLeftRight();
                checkTopBottom();
            }
        }
    }

    private function updateMask():void {
        super.mask = _masked ? super.addChild(baseMask) : null;
    }

    private function checkLeftRight():void {
        var leftValue:Number = AlignUtil.getValue(_left, contentWidth, ((alignRect ? alignRect : (_owner as Container).contentRect) as Rectangle).width);
        var rightValue:Number = AlignUtil.getValue(_right, contentWidth, ((alignRect ? alignRect : (_owner as Container).contentRect) as Rectangle).width);

        if(_left != null || _right != null) {
            if(_left != null && _right != null)	{
                x = leftValue;
                contentWidth = ((alignRect ? alignRect : (_owner as Container).contentRect) as Rectangle).width - leftValue - rightValue;
            } else if(_left != null)	{
                x = leftValue;
            } else if(_right != null)	{
                x = ((alignRect ? alignRect : (_owner as Container).contentRect) as Rectangle).width  - contentWidth - rightValue;
            }
        }
    }

    private function checkTopBottom():void {
        var topValue:Number = AlignUtil.getValue(_top, contentHeight, ((alignRect ? alignRect : (_owner as Container).contentRect) as Rectangle).height);
        var bottomValue:Number = AlignUtil.getValue(_bottom, contentHeight, ((alignRect ? alignRect : (_owner as Container).contentRect) as Rectangle).height);

        if(_top != null || _bottom != null) {
            if(_top != null && _bottom != null)	{
                y = topValue;
                contentHeight = ((alignRect ? alignRect : (_owner as Container).contentRect) as Rectangle).height - topValue - bottomValue;
            } else if(_top != null)	{
                y = topValue;
            } else if(_bottom != null)	{
                y = ((alignRect ? alignRect : (_owner as Container).contentRect) as Rectangle).height  - contentHeight - bottomValue;
            }
        }
    }

    private function checkChildsSizes():void {
        var i:int, count:int = childs.length, child:DisplayObject, size:Size, flag:Boolean = false;

        for(i = 0; i < count; i++) {
            child = childs[i];
            size = sizes[i];

            if(size.width != child.width || size.height != child.height) {
                size.width = child.width;
                size.height = child.height;

                flag = true;
            }
        }

        if(flag) {
            update();
        }
    }

    private function childEvents(target:DisplayObject, remove:Boolean = false):DisplayObject {
        if(target is Container) {
            if(remove) {
                target.removeEventListener(ContainerEvent.CONTENT_SIZE_CHANGE, child_ContentSizeChange_Handler);
            } else {
                target.addEventListener(ContainerEvent.CONTENT_SIZE_CHANGE, child_ContentSizeChange_Handler);
            }
        } else {
            if(remove) {
                var index:uint = childs.indexOf(target);

                if(index != -1) {
                    ArrayUtil.removeItem(childs, target);
                    ArrayUtil.removeItem(sizes, sizes[index]);
                }
            } else {
                childs.push(target);
                sizes.push(new Size(target.width, target.height));
            }
        }

        update();

        return target;
    }

    private function ownerEvents(target:DisplayObject, remove:Boolean = false):DisplayObject {
        if(target is Container) {
            if(remove) {
                target.removeEventListener(ContainerEvent.CONTENT_SIZE_CHANGE, child_ContentSizeChange_Handler);
            } else {
                target.addEventListener(ContainerEvent.CONTENT_SIZE_CHANGE, child_ContentSizeChange_Handler);
            }
        }

        update();

        return target;
    }

    // ---

    private function addedToStage_Handler(event:Event):void {
        removeEventListener(Event.ADDED_TO_STAGE, addedToStage_Handler);

        ready();
    }

    private function owner_contentSizeChange_Handler(event:ContainerEvent):void {
        update();
    }

    private function child_ContentSizeChange_Handler(event:ContainerEvent):void {
        update();
    }

}
}