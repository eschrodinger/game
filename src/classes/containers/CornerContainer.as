package classes.containers {
import classes.events.ContainerEvent;

public class CornerContainer extends Container {

    private var _corner:int;

    public function CornerContainer(_width:int, _height:int, _corner:int) {
        super(_width, _height, true);

        // ---

        this._corner = _corner;

        // ---

        addEventListener(ContainerEvent.CONTENT_SIZE_CHANGE, contentSizeChange_Handler);

        // ---

        update();
    }

    // ---

    public function get corner():int {
        return _corner;
    }

    public function set corner(value:int):void {
        var oldValue:int = _corner;

        _corner = value;


        if(oldValue != value) {
            update();

            dispatchEvent(new ContainerEvent(ContainerEvent.CONTENT_FORM_CHANGE));
        }

    }

    // ---

    private function update():void {
        if(created) {
            baseMask.graphics.clear();
            baseMask.graphics.beginFill(0, 0);
            baseMask.graphics.drawRoundRect(0, 0, contentWidth, contentHeight, _corner, _corner);
            baseMask.graphics.endFill();
        }
    }

    // ---

    private function contentSizeChange_Handler(event:ContainerEvent):void {
        update();
    }
}
}