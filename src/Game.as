package {
import classes.containers.Container;
import classes.dialog.DialogEvent;
import classes.dialog.DialogManager;

import components.Components;
import components.buttons.ButtonStart;
import components.dialogs.DialogAttempts;
import components.dialogs.DialogMachine;
import components.dialogs.DialogOverlay;
import components.dialogs.DialogTable;
import components.events.DialogAttemptsEvent;
import components.events.DialogMachineEvent;
import components.events.DialogTableEvent;

import flash.display.StageAlign;
import flash.display.StageScaleMode;
import flash.events.Event;
import flash.events.MouseEvent;

import helpers.Constants;

/**
 * @author Dimox
 */
public class Game extends Container {

    private static var _instance:Game;

    // ---

    private var buttonStart:ButtonStart;

    /**
     * Base <b>Game</b> class
     */
    public function Game() {
        super(0, 0, false);

        background = Constants.BG_GAME;
    } // end function

    // ---

    override public function initialize():void {
        _instance = this;

        initializeOptions();
        initializeGUI();
        initializeUsability();

        update();
    } // end function

    // ---

    /**
     * Get base class instance
     */
    public static function get instance():Game {
        return _instance;
    } // end function

    // ---

    /**
     * Initialize base class & stage options
     */
    private function initializeOptions():void {
        stage.align = StageAlign.TOP_LEFT;
        stage.scaleMode = StageScaleMode.NO_SCALE;

        stage.addEventListener(Event.RESIZE, stage_Resize_Handler);
    } // end function

    /**
     * Initialize gui components
     */
    private function initializeGUI():void {

        buttonStart = addChild(new ButtonStart()) as ButtonStart;
        buttonStart.verticalCenter = 0;
        buttonStart.horizontalCenter = 0;
        buttonStart.addEventListener(MouseEvent.CLICK, buttonStart_Click_Handler);

        // ---

        Components.dialogManager = stage.addChild(new DialogManager(contentWidth, contentHeight, false)) as DialogManager;
        Components.dialogManager.overlay = DialogOverlay;

        Components.dialogMachine = new DialogMachine(600, 560, 20);
        Components.dialogMachine.manager = Components.dialogManager;
        Components.dialogMachine.attempts = Constants.DEFAULT_ATTEMPTS;
        Components.dialogMachine.addEventListener(DialogMachineEvent.ATTEMPTS_CLICK, dialogMachine_AttemptsClick_Handler);
        Components.dialogMachine.addEventListener(DialogMachineEvent.TABLE_CLICK, dialogMachine_TableClick_Handler);
        Components.dialogMachine.addEventListener(DialogEvent.HIDE, dialogMachine_Hide_Handler);

        Components.dialogMachine.show();

        Components.dialogTable = new DialogTable(720, 500, 20);
        Components.dialogTable.manager = Components.dialogManager;
        Components.dialogTable.addEventListener(DialogTableEvent.CLOSE_CLICK, dialogTable_CloseClick_Handler);
        Components.dialogTable.addEventListener(DialogEvent.HIDE, dialogTable_Hide_Handler);

        Components.dialogAttempts = new DialogAttempts(600, 190, 20);
        Components.dialogAttempts.manager = Components.dialogManager;
        Components.dialogAttempts.addEventListener(DialogAttemptsEvent.NO_CLICK, dialogAttempts_NoClick_Handler);
        Components.dialogAttempts.addEventListener(DialogAttemptsEvent.YES_CLICK, dialogAttempts_YesClick_Handler);
        Components.dialogAttempts.addEventListener(DialogEvent.HIDE, dialogAttempts_Hide_Handler);
        Components.dialogAttempts.show();

    } // end function

    /**
     * Initialize usability
     */
    private function initializeUsability():void {

    } // end function

    private function update():void {
        resize(stage.stageWidth, stage.stageHeight);

        Components.dialogManager.resize(stage.stageWidth, stage.stageHeight);
    } // end function

    // ---

    private function stage_Resize_Handler(event:Event):void {
        update();
    } // end function

    private function buttonStart_Click_Handler(event:MouseEvent):void {
        Components.dialogManager.hideAll();
        Components.dialogMachine.show();
    } // end function

    // Dialog machine

    private function dialogMachine_AttemptsClick_Handler(event:DialogMachineEvent):void {
        Components.dialogManager.hideAll();
        Components.dialogAttempts.show();
    } // end function

    private function dialogMachine_TableClick_Handler(event:DialogMachineEvent):void {
        Components.dialogManager.hideAll();
        Components.dialogTable.show();
    } // end function

    private function dialogMachine_Hide_Handler(event:DialogEvent):void {
    } // end function

    // Dialog table

    private function dialogTable_CloseClick_Handler(event:DialogTableEvent):void {

    } // end function

    private function dialogTable_Hide_Handler(event:DialogEvent):void {
        Components.dialogMachine.show();
    } // end function

    // Dialog attempts

    private function dialogAttempts_NoClick_Handler(event:DialogAttemptsEvent):void {

    } // end function

    private function dialogAttempts_YesClick_Handler(event:DialogAttemptsEvent):void {
        Components.dialogMachine.attempts += Constants.COUNT_ATTEMPTS;
    } // end function

    private function dialogAttempts_Hide_Handler(event:DialogEvent):void {
        Components.dialogMachine.show();
    } // end function

}
}