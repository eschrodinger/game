package components.buttons {
import classes.Button;
import classes.containers.AutoContainer;
import classes.containers.Container;

import flash.geom.ColorTransform;

import helpers.Constants;
import helpers.Resource;

public class ButtonClose extends Button {

    public function ButtonClose() {
        super(0, 0, 0);

        stateUp = generateState(STATE_UP);
        stateOver = generateState(STATE_OVER);
        stateDown = generateState(STATE_DOWN);
        stateDisabled = generateState(STATE_DISABLED);
    }

    private function generateState(stateName:String):Container {
        var container:AutoContainer = new AutoContainer(new Resource.close);

        // ---

        contentSize = container.contentSize;

        // ---

        switch(stateName) {
            case STATE_UP:
                break;

            case STATE_OVER:
                container.filters = [Constants.CONTRAST_FILTER];
                break;

            case STATE_DOWN:
                container.transform.colorTransform = new ColorTransform(0.9, 0.9, 0.9);
                break;

            case STATE_DISABLED:
                container.filters = [Constants.BLACKOUT_FILTER];
                break;

            default:
                break;
        }

        return container;
    }

}
}
