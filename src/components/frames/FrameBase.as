package components.frames {
import classes.containers.CornerContainer;
import classes.events.ContainerEvent;

import flash.display.Bitmap;
import flash.geom.Matrix;

import helpers.Resource;

public class FrameBase extends CornerContainer {

    private var matrix:Matrix;

    public function FrameBase(_width:int, _height:int, _corner:int) {
        super(_width, _height, _corner);

        // ---

        matrix = new Matrix();

        // ---

        addEventListener(ContainerEvent.CONTENT_FORM_CHANGE, contentFormChange_Handler);

        update();
    }

    // ---

    private function update():void {
        graphics.clear();

        graphics.beginFill(0xE0EFF7);
        graphics.drawRoundRect(0, 0, contentWidth, contentHeight, corner, corner);
        graphics.endFill();

        graphics.beginBitmapFill((new Resource.border_left as Bitmap).bitmapData);
        graphics.drawRect(0, 0, 11, contentHeight);
        graphics.endFill();

        matrix.tx = contentWidth - 11;
        matrix.ty = 0;
        graphics.beginBitmapFill((new Resource.border_right as Bitmap).bitmapData, matrix);
        graphics.drawRect(contentWidth - 11, 0, 11, contentHeight);
        graphics.endFill();

        graphics.beginBitmapFill((new Resource.border_top as Bitmap).bitmapData);
        graphics.drawRect(0, 0, contentWidth, 11);
        graphics.endFill();

        matrix.tx = 0;
        matrix.ty = contentHeight - 11;
        graphics.beginBitmapFill((new Resource.border_bottom as Bitmap).bitmapData, matrix);
        graphics.drawRect(0, contentHeight - 11, contentWidth, 11);
        graphics.endFill();
    }

    // ---

    private function contentFormChange_Handler(event:ContainerEvent):void {
        update();
    }

}
}
