package components.frames {
import classes.containers.CornerContainer;
import classes.events.ContainerEvent;

public class FrameBorder extends CornerContainer {

    public function FrameBorder(_width:int, _height:int, _corner:int) {
        super(_width, _height, _corner);

        // ---

        addEventListener(ContainerEvent.CONTENT_FORM_CHANGE, contentFormChange_Handler);

        update();
    }

    // ---

    private function update():void {
        graphics.clear();

        graphics.beginFill(0x681768);
        graphics.drawRoundRect(0, 0, contentWidth, contentHeight, corner, corner);
        graphics.endFill();

        graphics.beginFill(0xE8B317);
        graphics.drawRoundRect(1, 1, contentWidth - 2, contentHeight - 2, corner - 2, corner - 2);
        graphics.endFill();

        graphics.beginFill(0x77744D);
        graphics.drawRoundRect(3, 3, contentWidth - 6, contentHeight - 6, corner - 4, corner - 4);
        graphics.endFill();

        graphics.beginFill(0xE0EFF7);
        graphics.drawRoundRect(5, 5, contentWidth - 10, contentHeight - 10, corner - 6, corner - 6);
        graphics.endFill();
    }

    // ---

    private function contentFormChange_Handler(event:ContainerEvent):void {
        update();
    }

}
}
