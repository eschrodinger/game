package components {
import classes.TextLabel;
import classes.containers.AutoContainer;
import classes.containers.CornerContainer;
import classes.containers.VGroupContainer;
import classes.events.ContainerEvent;
import classes.geometry.Size;
import classes.utils.AlignUtil;
import classes.utils.DrawUtil;
import classes.utils.SpecialUtil;

import flash.display.Bitmap;

import helpers.Resource;

public class WidgetAttempts extends CornerContainer {

    private var groupContainer:VGroupContainer;

    private var borderContgainer:AutoContainer;
    private var iconContgainer:AutoContainer;

    private var icon:Bitmap;

    private var label:TextLabel;

    public function WidgetAttempts(_width:int, _height:int, _corner:int) {
        super(_width, _height, _corner);

        // ---

        groupContainer = addChild(new VGroupContainer(false)) as VGroupContainer;
        groupContainer.verticalCenter = 2;
        groupContainer.horizontalCenter = -2;
        groupContainer.gap = 2;
        groupContainer.hAlign = AlignUtil.CENTER;

        borderContgainer = groupContainer.addChild(new AutoContainer(new Resource.borderLightBlue)) as AutoContainer;

        icon = new Resource.attempts;
        icon.smoothing = true;

        SpecialUtil.autoFitProportion(icon, new Size(icon.width - 10, icon.height - 10), false);

        iconContgainer = borderContgainer.addChild(new AutoContainer(icon)) as AutoContainer;
        iconContgainer.verticalCenter = 0;
        iconContgainer.horizontalCenter = 0;

        label = groupContainer.addChild(new TextLabel("HNCB")) as TextLabel;
        label.size = 18;
        label.color = 0xFBFB9C;
        label.text = "Попытки";

        // ---

        addEventListener(ContainerEvent.CONTENT_FORM_CHANGE, contentFormChange_Handler);

        update();
    }

    // ---

    private function update():void {
        graphics.clear();

        graphics.beginFill(0x681768);
        graphics.drawRoundRect(0, 0, contentWidth, contentHeight, corner, corner);
        graphics.endFill();

        DrawUtil.fillGradiend(graphics, contentWidth - 2, contentHeight, Math.PI / 2, [0xCD97AF, 0x7C4D7B], [1, 1]);
        graphics.drawRoundRect(0, 0, contentWidth - 2, contentHeight, corner, corner);
        graphics.endFill();
    }

    // ---

    private function contentFormChange_Handler(event:ContainerEvent):void {
        update();
    }

}
}
