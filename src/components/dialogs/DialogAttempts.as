package components.dialogs {
import classes.TextLabel;
import classes.containers.Container;
import classes.containers.HGroupContainer;
import classes.events.ContainerEvent;
import classes.utils.DrawUtil;

import components.WidgetAttempts;

import components.buttons.ButtonBlue;
import components.buttons.ButtonRed;

import flash.display.Bitmap;

import flash.events.MouseEvent;

import flashx.textLayout.formats.TextAlign;

import helpers.Console;

import helpers.Constants;
import helpers.Resource;

public class DialogAttempts extends DialogBase {

    private var leftContainer:Container;

    private var widgetAttempts:WidgetAttempts;

    // ---

    private var rightContainer:Container;

    private var labelInfo:TextLabel;

    private var buttonGroup:HGroupContainer;
    private var buttonNo:ButtonRed;
    private var buttonYes:ButtonBlue;

    public function DialogAttempts(_width:int, _height:int, _corner:int) {
        super(_width, _height, _corner);

        // ---

        leftContainer = addChild(new Container(210, 0, false)) as Container;

        widgetAttempts = leftContainer.addChild(new WidgetAttempts(0, 0, 0)) as WidgetAttempts;
        widgetAttempts.x = 5;
        widgetAttempts.y = 5;
        // ---

        rightContainer = addChild(new Container(0, 0, false)) as Container;
        rightContainer.x = leftContainer.contentWidth;

        // ---

        labelInfo = rightContainer.addChild(new TextLabel()) as TextLabel;
        labelInfo.maxLines = 2;
        labelInfo.size = 17;
        labelInfo.color = 0x14519D;
        labelInfo.text = "Вы собираетесь добавить " + Constants.COUNT_ATTEMPTS + " попыток.\nПродолжить?";
        labelInfo.textAlign = TextAlign.CENTER;
        labelInfo.horizontalCenter = 0;

        // ---

        buttonGroup = rightContainer.addChild(new HGroupContainer(false)) as HGroupContainer;
        buttonGroup.gap = 20;
        buttonGroup.horizontalCenter = 0;

        // ---

        buttonNo = buttonGroup.addChild(new ButtonRed()) as ButtonRed;
        buttonNo.text = "Нет";
        buttonNo.addEventListener(MouseEvent.CLICK, buttonNo_Click_Handler);

        // ---

        buttonYes = buttonGroup.addChild(new ButtonBlue()) as ButtonBlue;
        buttonYes.text = "Да";
        buttonYes.addEventListener(MouseEvent.CLICK, buttonYes_Click_Handler);

        // ---

        addEventListener(ContainerEvent.CONTENT_FORM_CHANGE, contentFormChange_Handler);

        update();
    }

    // ---

    private function update():void {

        leftContainer.contentHeight = contentHeight;

        leftContainer.graphics.clear();
        leftContainer.graphics.beginFill(0xE0EFF7);
        DrawUtil.drawRoundRect(leftContainer.graphics, 5, 5, leftContainer.contentWidth - 5, leftContainer.contentHeight - 10, corner - 6, corner - 6, true, false, false, true);
        leftContainer.graphics.endFill();

        widgetAttempts.resize(leftContainer.contentWidth - 5, leftContainer.contentHeight - 10);
        widgetAttempts.corner = corner - 10;

        // ---

        rightContainer.resize(contentWidth - leftContainer.contentWidth, contentHeight);

        rightContainer.graphics.clear();
        rightContainer.graphics.beginBitmapFill((new Resource.border_left as Bitmap).bitmapData);
        rightContainer.graphics.drawRect(0, 5, 11, contentHeight - 10);
        rightContainer.graphics.endFill();

        // ---

        buttonGroup.bottom = - buttonGroup.contentHeight / 2 + 4;

        // ---

        labelInfo.verticalCenter = -(buttonGroup.contentHeight / 4);
        labelInfo.maxContentWidth = rightContainer.contentWidth;
    }

    // ---

    private function contentFormChange_Handler(event:ContainerEvent):void {
        update();
    }

    private function buttonNo_Click_Handler(event:MouseEvent):void {

    }

    private function buttonYes_Click_Handler(event:MouseEvent):void {

    }

}
}
