package components.dialogs {
public class DialogMachine extends DialogBase {

    private var _attempts:int

    public function DialogMachine(_width:int, _height:int, _corner:int) {
        super(_width, _height, _corner);
    }

    public function get attempts():int {
        return _attempts;
    }

    public function set attempts(value:int):void {
        _attempts = value;
    }
}
}
