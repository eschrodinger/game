package components.dialogs {
import classes.containers.CornerContainer;
import classes.dialog.Dialog;
import classes.events.ContainerEvent;
import classes.geometry.Size;
import classes.utils.DrawUtil;

import components.buttons.ButtonClose;
import components.frames.FrameBase;
import components.frames.FrameBorder;

import flash.display.Bitmap;
import flash.geom.Matrix;

import helpers.Resource;

public class DialogBase extends Dialog {

    private var border:FrameBorder;
    private var bg:FrameBase;

    private var button:ButtonClose;

    public function DialogBase(_width:int, _height:int, _corner:int) {
        super(_width, _height, _corner);

        // ---

        horizontalCenter = 0;
        verticalCenter = 0;

        // ---

        border = addChildAt(new FrameBorder(0, 0, 0), 0) as FrameBorder;
        border.left = 0;
        border.right = 0;
        border.top = 0;
        border.bottom = 0;

        // ---

        bg = addChildAt(new FrameBase(0, 0, 0), 1) as FrameBase;
        bg.left = 5;
        bg.right = 5;
        bg.top = 5;
        bg.bottom = 5;

        // ---

        button = closeButton.addChild(new ButtonClose()) as ButtonClose;

        closeButton.mouseChildren = true;
        closeButton.contentSize = button.contentSize;
        closeButton.top = -closeButton.contentHeight / 2 + 6;
        closeButton.right = -closeButton.contentWidth / 2 + 10;

        // ---

        addEventListener(ContainerEvent.CONTENT_FORM_CHANGE, contentFormChange_Handler);

        update();
    }

    // ---

    private function update():void {
        border.corner = corner;

        // ---

        bg.corner = corner - 8;
    }

    // ---

    private function contentFormChange_Handler(event:ContainerEvent):void {
        update();
    }

}
}
