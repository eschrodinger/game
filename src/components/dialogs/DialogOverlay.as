package components.dialogs {
import classes.containers.Container;

import helpers.Constants;

public class DialogOverlay extends Container {
    public function DialogOverlay(_width:int, _height:int) {
        super(_width, _height, true);

        // ---

        mouseEnabled = true;

        background = Constants.BG_DIALOG_OVERLAY;
    }

}
}