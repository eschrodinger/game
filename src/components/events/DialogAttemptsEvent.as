package components.events {
import classes.base.BaseEvent;

import flash.events.Event;

public class DialogAttemptsEvent extends BaseEvent {

    public static const YES_CLICK:String = "DialogAttemptsEvent:YES_CLICK";
    public static const NO_CLICK:String = "DialogAttemptsEvent:NO_CLICK";

    public function DialogAttemptsEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false) {
        super(type, bubbles, cancelable);
    }

    override public function clone():Event {
        var newEvent:DialogAttemptsEvent = new  DialogAttemptsEvent(type, bubbles, cancelable);

        return newEvent;
    }

}
}
