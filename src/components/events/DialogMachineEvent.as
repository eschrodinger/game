package components.events {
import classes.base.BaseEvent;

import flash.events.Event;

public class DialogMachineEvent extends BaseEvent {

    public static const ATTEMPTS_CLICK:String = "DialogMachineEvent:ATTEMPTS_CLICK";
    public static const TABLE_CLICK:String = "DialogMachineEvent:TABLE_CLICK";

    public function DialogMachineEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false) {
        super(type, bubbles, cancelable);
    }

    override public function clone():Event {
        var newEvent:DialogMachineEvent = new  DialogMachineEvent(type, bubbles, cancelable);

        return newEvent;
    }

}
}
