package components.events {
import classes.base.BaseEvent;

import flash.events.Event;

public class DialogTableEvent extends BaseEvent {

    public static const CLOSE_CLICK:String = "DialogTableEvent:CLOSE_CLICK";

    public function DialogTableEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false) {
        super(type, bubbles, cancelable);
    }

    override public function clone():Event {
        var newEvent:DialogTableEvent = new  DialogTableEvent(type, bubbles, cancelable);

        return newEvent;
    }

}
}
