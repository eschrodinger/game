package components {
import classes.dialog.DialogManager;

import components.dialogs.DialogAttempts;
import components.dialogs.DialogMachine;
import components.dialogs.DialogTable;

public class Components {
    public function Components() {
    }

    public static var dialogManager:DialogManager;

    public static var dialogMachine:DialogMachine;
    public static var dialogTable:DialogTable;
    public static var dialogAttempts:DialogAttempts;

}
}
